# The Penumbra GUI
The penumbra GUI is made using ReactJS alongside Electron.

## Installation


1. Consolidate with the [NodeJS documentation](https://docs.npmjs.com/downloading-and-installing-node-js-and-npm) for instructions on how to install Node and npm on your computer.
2. Use npm to [install yarn](https://classic.yarnpkg.com/lang/en/docs/install/#debian-stable)


---

## Installing the required software for the GUI
1. Open your terminal
2. Make sure you are inside the Penumbra_GUI folder
3. Run ```yarn install```
  - Alot of warnings may appear. These can safely be ignored

---

## Running the GUI in the development server
1. Run ```yarn start``` (Will take some time and requires an internet connection)
2. Voila! The GUI should now appear

> **NOTE**
> The penumbra cmake project must be compiled and be located in the parent folder of Penumbra_GUI


## Compiling the application to an executable
1. Run ```yarn build```
2. Inside the dist folder you should now have either a: *.exe* file (windows) or an .appimage file (linux)
3. Run this file to run the program