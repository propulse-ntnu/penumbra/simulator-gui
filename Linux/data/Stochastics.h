//
// Created by 3rlen on 25.01.2022.
//

#ifndef PENUMBRA_STOCHASTICS_H
#define PENUMBRA_STOCHASTICS_H


#include <boost/property_tree/json_parser.hpp>
namespace pt = boost::property_tree;

struct Stochastics{
    /*
     Struct for holding all stochastic values given by stochastic json.
     */
    //Rocket object:
    double sigma_engine, sigma_chute;

    //Pink noise:
    double ALPHA, DELTA_T, STDDEV;
    int NUMBER_OF_POLES;

    //Wind
    double avg_wind_speed, wind_speed_sd, avg_wind_direction, wind_direction_sd;
    bool random_direction;

    Stochastics(const std::string& path){
        //Reading json:
        pt::ptree root;
        pt::read_json(path, root);

        //rocket:
        sigma_engine = root.get<double>("sigma_engine");
        sigma_chute = root.get<double>("sigma_chute");

        //wind:
        avg_wind_speed = root.get<double>("wind.avg_wind_speed");
        wind_speed_sd = root.get<double>("wind.wind_speed_sd");
        avg_wind_direction = root.get<double>("wind.avg_wind_direction");
        wind_direction_sd = root.get<double>("wind.wind_direction_sd");
        random_direction = root.get<bool>("wind.random_direction");

        //pink noise:
        if(root.get<std::string>("pink_noise.ALPHA") == "3/5"){
            ALPHA = 3.0/5.0; //This is the standard case. An alpha of 3./5 creates a turbulence/frequency similar to
            // what to that is expected in the atmosphere. 1/f^{3/5}
        } else {
            ALPHA = 3./5; //just in case..
            std::cout << "ALPHA (pink noise) value in stochastics json path is not recognized\n setting alpha as 3/5..\n";
        }
        NUMBER_OF_POLES = root.get<int>("pink_noise.NUMBER_OF_POLES");
        DELTA_T = root.get<double>("pink_noise.DELTA_T");
        STDDEV = root.get<double>("pink_noise.STDDEV");
    }
};

#endif //PENUMBRA_STOCHASTICS_H
