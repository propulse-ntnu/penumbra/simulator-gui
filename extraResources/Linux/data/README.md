# Data
This is our sweeeet database :) This folder contains the files needed to initialise a simulation with a engine and a rocket. As well as c++ 
code to read data from these files. 

### motors
holds .xml files for engines.

### rockets
holds rocket files.

### EngineInputData
deals with the xml files with a boost xml parser and a boost property tree. 
This way we can read engine input data and represent a engine object.

