# UI Documentation
This document/folder is going to describe how UI is setup for Penumbra.

## Technology
The application is essentially made as a website using ReactJS, Javascript, CSS (SCSS more specifically). To run this application locally, the library Electron is being used.

## Project overview
| Folder | What|
|--------|------|
| [Public folder](./public/public.md) | Contains mainly electron logic. That would be interaction with the operating system |
[src](./src/src.md) | Contains UI logic. I.e pages, components and threejs logic

