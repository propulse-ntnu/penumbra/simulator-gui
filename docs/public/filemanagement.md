# Filemanagement
Files are read in the FileManager file which can be found inside the util folder.

It is created to parse several average#.csv files in addition to one events.csv file. The data is stored in a datastructure as described:

```json
{
	"averages": [avg1, avg2, ..., avgN],
	"events": [event1, event2, ..., eventN]
}
```

avgN files are formatted as such:

```json
{
	T: 1046.77
	Vw: 48.5684
	a: 0
	e: NaN
	mass: 39.15057
	p: 0
	q: 0
	q1: -0.000685233
	q2: 0.026168
	q3: 0.026168
	q4: 0.999315
	r: 0
	t: 0.01
	v: 0.13367363899608706
	vx: -0.00699591
	vy: -0.00699591
	vz: 0.133307
	x: -0.0000233186
	y: -0.0000233186
	z: 0.000444335
}
```

eventN files are formatted as such. Currently we only write one event file based on avg1.csv:

```json
	"Apogee": {
		"time": 10,
		"value": 10
	},
	"Max speed": {
		"value": 10
	},
	"Touchdown": {
		"time": 15,
		"velocity": 12,
		"y": 0,
		"z": 0
	}
```

## Where to find these in the code?
In ResultPage.js we have 2 states: fileData and eventData

fileData currently contains the first average file (we did this for testing). 

eventData currently contains the events for the first simulation