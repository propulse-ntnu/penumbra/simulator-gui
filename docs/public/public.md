# The public folder
This folder contains all the underlying logic for the app to work. For instance OS communication, and also how electron should be setup.

## Folder overview
| Folder | What it should contain |
|--------|------------------------|
[assets](../../public/assets) | Contains the different assets used by the app, which the app needs to read in real time. We have to have this folder in public since this is where the app is compiled. |
[util](../../public/util) | Mostly utilitarian functions for OS communication used in electron.js