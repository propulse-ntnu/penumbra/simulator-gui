# The src folder
This folder contains all the UI logic in the app, and is hence the largest folder in the project

## Folder overview

| Folder  | What it should contain |
|---------|------------------------|
[Components](../../src/components) | Contains the logic for the different reusable components on the app. These components should be as small as possible. | 
[Containers](../../src/containers) | Contains the containers for the UI. These containers can be as large as entire pages. | 
[static](../../src/static) | Contains the files which will not change during execution of the app. I.e pictures and CSS files | 
[Threejs](../../src/Threejs) | Contains logic for threejs 3D renderings used in the app