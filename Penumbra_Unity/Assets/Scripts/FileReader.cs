using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;


public class FileReader : MonoBehaviour
{
  public static string path = Environment.GetFolderPath(Environment.SpecialFolder.UserProfile) + "/PenumbraProjects/results/test.csv";

  private static Dictionary<int, string> readHeaderLine(string headerLine) {
    string line = headerLine; //To remove new line char
    string[] lineArr = line.Split(',');
    Dictionary<int, string> headerObj = new Dictionary<int, string>();
    for(int i = 0; i < lineArr.Length; i++){
      headerObj[i] = lineArr[i];
    }
    return headerObj;
  }

  public static List<Dictionary<string, float>> LoadFile(){
    Debug.Log("Path: " + path);
    StreamReader reader = new StreamReader(path);
    string fileContent = reader.ReadToEnd();
    return(LoadFileContent(fileContent));
  }

  public static List<Dictionary<string, float>> LoadFileContent(string content){
    string fileContent = content;
    List<Dictionary<string, float>> fileData = new List<Dictionary<string, float>>();
		List<Dictionary<string, float>> eventData = new List<Dictionary<string, float>>();

		List<Dictionary<string, float>> fileList = new List<Dictionary<string, float>>();

    string[] lines = fileContent.Split('\n');
    Dictionary<int, string> headerMap = readHeaderLine(lines[0]); //Read the headers, setting the correct values in the object
		Dictionary<string, float> dataMap;
		float last_time = 0;
    for(int i=1; i<lines.Length-1; i++) {
      
			string line = lines[i];

			dataMap = new Dictionary<string, float>();

			//If we have an event
			// if (line.Contains("#BURNOUT")) {
			// 	Debug.Log("Burnout time: " + last_time);
			// 	dataMap.Add("BURNOUT", 1);
			// 	dataMap.Add("t", last_time);
			// 	fileData.Add(dataMap);
			// 	continue;
			// }
			if (line.Contains("#")) {
				continue;
			}
      try {
        string[] lineArr = line.Split(',');
        last_time = float.Parse(lineArr[0]);
        for(int j=0; j<headerMap.Keys.Count; j++) {
          try {
            float num = float.Parse(lineArr[j]);
            dataMap.Add(headerMap[j], num);
          } catch {
            dataMap.Add(headerMap[j], 0);
          }
        }
        fileData.Add(dataMap);
      } catch {
        Debug.Log("Could not add line " + line);
        continue;
      }

    }
    Debug.Log("Loaded " + fileData.Count + " datapoints");
    return fileData;
  }
}
