using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkyGeneration : MonoBehaviour
{
    public GameObject cloudPrefab;
    public float radius;
    public int cloudCount = 1000;
    
    public int min_height, max_heigth;
    public int width, radius_generation;

    void Start()
    {
      Vector3 pos;
      for(int i = 0; i < cloudCount; i++) {
        while(true) {
          pos = Random.onUnitSphere * (radius + Random.Range(min_height, max_heigth)) + new Vector3(0,-radius,0);
          if(pos.y > -radius+radius_generation) {
            Instantiate(cloudPrefab, pos, this.transform.rotation, this.transform);
            break;
          }
        }
      }
    }
}
