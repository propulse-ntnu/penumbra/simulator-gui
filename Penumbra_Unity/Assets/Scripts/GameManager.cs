using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : MonoBehaviour
{
    public GameObject rocket;

    public List<Dictionary<string, float>> simulationData;

    // Start is called before the first frame update
    void Start()
    {
			Debug.Log("Starting game");
			if(false) {
				simulationData = FileReader.LoadFile();

				GameObject instantiatedRocket = Instantiate(rocket);
			}
    }

    public void LoadFile(string fileContent) {
      Debug.Log("Reading filecontent");
      simulationData = FileReader.LoadFileContent(fileContent);

      GameObject instantiatedRocket = Instantiate(rocket);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
