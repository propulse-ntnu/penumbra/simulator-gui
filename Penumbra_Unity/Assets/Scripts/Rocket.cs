using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Rocket : MonoBehaviour
{

    private GameObject gameManager;
    private List<Dictionary<string, float>> dataPoints;
		private float burnout_time;
    private GameObject world;
    private int tick = 0;

    private Text altDisplay;
    private Text velDisplay;

		public GameObject particles;


    // Start is called before the first frame update
    void Start()
    {
      gameManager = GameObject.Find("GameManager");
      dataPoints = gameManager.GetComponent<GameManager>().simulationData;
      altDisplay = GameObject.Find("AltText").GetComponent<Text>();
      velDisplay = GameObject.Find("VelText").GetComponent<Text>();

      world = GameObject.FindWithTag("World");
    }

    // Update is called once per frame
    void Update()
    {
        Dictionary<string, float> currentPoint = dataPoints[tick];
				if(currentPoint.ContainsKey("BURNOUT")) {
					Debug.Log("BURNOUT REACHED");
					particles.SetActive(false);
					tick++;
					return;
				}

        altDisplay.text = currentPoint["z"].ToString("000 000");
        velDisplay.text = currentPoint["vz"].ToString("000 000");

        world.transform.position = new Vector3(currentPoint["x"], -currentPoint["z"], currentPoint["y"]);
        this.transform.rotation = new Quaternion(currentPoint["q1"], currentPoint["q2"], currentPoint["q3"], currentPoint["q4"]);
        tick++;

    }
}
