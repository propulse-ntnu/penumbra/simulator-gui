import React from "react";
import { Route, Switch, HashRouter, withRouter } from "react-router-dom";

import ErrorPage from "./containers/ErrorPage";
import LandingPage from "./containers/LandingPage";
import ProjectPage from "./containers/ProjectPage/ProjectPage";
import SimulationPage from "./containers/SimulationPage";
import ResultPage from "./containers/ResultPage/ResultPage";
import MultiResultPage from "./containers/MultiResultPage/MultiResultPage";
import RealTimePage from "./containers/RealtimePage";
import { Navbar } from "./components/Navbar/Navbar/Navbar";
import { NavbarMulti } from "./components/Navbar/Navbar/NavbarMulti";

function Routes() {
  return(
    <HashRouter>
				<Switch>
          <Route exact path="/error">
            <ErrorPage />
          </Route>
					<Route exact path="/">
            <LandingPage />
					</Route>
          <Route exact path="/project/home"> 
            <ProjectPage />
          </Route>
          <Route exact path="/project/results">
            <Navbar />
            <ResultPage />
          </Route>
          <Route exact path="/project/multiresults">
            <NavbarMulti />
            <MultiResultPage /> 
          </Route>
          <Route exact path="/simulation/:multisim">
            <SimulationPage />
          </Route>
          <Route exact path="/project/realtime">
            <Navbar />
            <RealTimePage />
          </Route>
          <Route exact path="/project/multirealtime">
            <NavbarMulti />
            <RealTimePage />
          </Route>
				</Switch> 
		</HashRouter>
  );
}
//Might need to export with router
export default Routes;
