import {createGlobalStyle} from "styled-components";

const colors = {
	background: "#0E101D"
}

export const GlobalStyles = createGlobalStyle`
	body {
		background-color: ${colors.background};
		margin: 0px;
		padding: 0px;
	}

	img{
		width: 100%;
	}
`