import React from 'react';
import Routes from './Routes'

import "./static/css/main.css"
import "./static/css/buttons.css"
import "./static/css/text.css"
import "./static/css/components.css"
import "./static/css/containers.css"
import "./static/css/3d.css"

import { GlobalStyles } from './GlobalStyles.styles';


function App() {
  return (
    <div>
			<GlobalStyles />
      <Routes />
    </div>
  )
}

export default App;
