import { useLocation, useHistory } from 'react-router-dom'
import CTAButton from '../components/CTAButton';

export default function ErrorPage(props){
  let history = useHistory();
  let location = useLocation();

  return(
    <div className="simulation-page">
      <h1>Error page</h1>
      <p className="subtext">You were redirected to this page because an error occured.</p>
      <p>Error: {location.state.message}</p> 
      <CTAButton text="Return to project" onClick={() =>  history.push("/project/home")}/>
    </div>
  )
}