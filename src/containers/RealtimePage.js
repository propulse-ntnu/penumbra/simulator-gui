import { useState, useEffect } from "react";
import Loader from "../components/loader";

import Unity, { UnityContext } from "react-unity-webgl";
import CTAButton from "../components/CTAButton";

const { ipcRenderer } = window.require("electron");


var unityContext = new UnityContext({
  loaderUrl: "Build/Alfa 1.0.loader.js",
  dataUrl: "Build/Alfa 1.0.data",
  frameworkUrl: "Build/Alfa 1.0.framework.js",
  codeUrl: "Build/Alfa 1.0.wasm",
});;



export default function RealTimePage() {
  const [fileContent, setFileContent] = useState();
  const [rocketData, setRocketData] = useState(null);
  const [homedir, setHomeDir] = useState(null);
  const [loading, setLoading] = useState(false);

  // Get default-config stored by the simulation
  useEffect(() => {

    ipcRenderer.on('app:file-string-reply', (event, arg) => {
      setFileContent(arg);
      setLoading(false);
    })

     ipcRenderer.on('app:no-file', (event, message) => {
     })
    ipcRenderer.send('app:get-file-string', 'pls');
  }, []);

  const sendHome = () => {
    console.log("Sending command to gamecontroller")
    unityContext.send("GameManager", "LoadFile", fileContent)
  }


  if(!loading){
    return(
      <div className="fullscreen default-page">
        <div className="unity-container">
          <Unity unityContext={unityContext} />
        </div>
        <CTAButton onClick={sendHome} text="Start" />
      </div>
    )
  } 

  return(
    <div className="simulation-page">
    <h1>Loading</h1>
    <Loader />
    <p className="subtext">Looking into simulation data</p>
  </div>
  )
}