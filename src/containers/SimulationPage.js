import { useState,useEffect } from "react";
import { useHistory, Redirect, useParams } from "react-router-dom"
import Loader from '../components/loader';

const { ipcRenderer } = window.require("electron");

export default function SimulationPage() {
  const [loading, setLoading] = useState(true)

  const { multisim } = useParams();
  console.log("Using multisim: ", multisim);

  const history = useHistory();
  if(!loading){
    console.log("Ready to push to projects")
    if(multisim) {
      console.log("Pushing to multiresults")
      history.push("/project/multiresults")
    } else {
      history.push("/project/results")
    }
  }

  useEffect(() => {
    //Send to error page if sim didn't succeed
    ipcRenderer.on('app:sim-failed', (event, message) => {
      history.push({
        pathname: '/error',
        state: {
          message: message
        }
      })
    })

    // When sim is finsihed, then stop loading
    ipcRenderer.on('app:sim-finished', (event, arg) => {
      setLoading(false)
    })

    // Request electron to run the simulation command
    ipcRenderer.send('app:run-sim', 'REQ: Start sim');
  }, []);

  return(
    <div className="simulation-page">
      <h1>Simulating</h1>
      <Loader />
      <p className="subtext">Please wait...</p>
    </div>
  );
}