import { useState, useEffect } from "react";
import { useHistory } from "react-router-dom";

import Loader from "../../components/loader";
import MultiLineChart from "../../components/MultiLineChart/MultiLineChart";
import InformationList from "../../components/InformationList/informationList";
import InformationListEntry from "../../components/InformationListEntry/informationListEntry";
import CTAButton from "../../components/CTAButton";
import MultiThreeDChart from "../../components/MultiThreeDChart/MultiThreeDChart";
import Card from "../../components/Card/Card";
import TabbedChartContainer from "../../components/TabbedChartDisplay/TabbedChartDisplay";

import { ReactComponent as GraphIcon } from "../../static/icons/show_chart_black_48dp.svg";
import { ReactComponent as DataIcon } from "../../static/icons/analytics_black_48dp.svg";
import { ReactComponent as SettingsIcon } from "../../static/icons/settings_black_24dp.svg"
import { DashboardStyled, MultiResultPageStyled, AbsoluteWrapper } from "./MultiResultPage.style";
import { MultiCustomLineChart } from "../../components/MultiCustomLineChart/MultiCustomLineChart";
import { BarChart } from "../../components/BarChart/BarChart";

const { ipcRenderer } = window.require("electron");


export default function MultiResultPage(){
  const [fileData, setFileData] = useState();
  const [eventData, setEventData] = useState();
  const [noFile, setNoFile] = useState(false);
  const [apogee, setApogee] = useState(0);
  const [maxVelocity, setMaxVelocity] = useState(0);
  const [maxAcceleration, setMaxAcceleration] = useState(0);
  const [simulationAverages, setsimulationAverages] = useState();
  const [simulationEvents, setSimulationEvents] = useState();

  console.log("Welcome to multiresultpage")

  // Get data stored by the simulation
  useEffect(() => {
    ipcRenderer.on('app:file-reply', (event, arg) => {
      console.log("File read:", arg);
      
      const simulationData = arg.averages;
      const events = arg.events;
      
      let tmpApogee = 0
      let tmpMaxVel = 0
      let tmpMaxAccel = 0;
      let simulationAverageKeys = [];
      let simulationEventKeys = [];

      console.log("Apogee time: ", events["event1"]["Apogee"]["time"])
      console.log("Apogee value: ", events["event1"]["Apogee"]["value"])
      console.log("Touchdown time: ", events["event1"]["Touchdown"]["time"])
      console.log("Touchdown value: ", events["event1"]["Touchdown"]["velocity"])

      // Normalize velocity, calculate acceleration, and add them to arg
      Object.keys(simulationData).forEach(simulation => {
        for (var point = 0; point < simulationData[simulation].length; point++) {
          
          // Calculation and setting velocity
          var v = Math.sqrt(simulationData[simulation][point].vx**2 + simulationData[simulation][point].vy**2 + simulationData[simulation][point].vz**2)
          simulationData[simulation][point].v = v

          // Calculating and setting acceleration
          if (point == 0) {
            var a = 0;
          } else {
            var v0 = simulationData[simulation][point - 1].v
            var v1 = v
            var delta_t = simulationData[simulation][point].t - simulationData[simulation][point - 1].t;
            var a = (v1 - v0) / delta_t;
          }
          simulationData[simulation][point].a = a

          // Stats                                       
          var z = simulationData[simulation][point].z    
          if(z && a && v) {
            if (z > tmpApogee) {tmpApogee = z}            
            if (v > tmpMaxVel) {tmpMaxVel = v}
            if (a > tmpMaxAccel) {tmpMaxAccel = a}
          }
        }

        // Saving simulationAverageKeys
        simulationAverageKeys.push(simulation);
      })

      // Saving simulationEventKeys
      Object.keys(events).forEach(simulation => {simulationEventKeys.push(simulation)});
      
      setApogee(tmpApogee);
      setMaxVelocity(tmpMaxVel);
      setMaxAcceleration(tmpMaxAccel);
      setsimulationAverages(simulationAverageKeys);
      setSimulationEvents(simulationEventKeys);
      setFileData(simulationData)
      setEventData(events);
    })

    ipcRenderer.on('app:no-file', (event, message) => {
      setNoFile(true)
    })

    ipcRenderer.send('app:request-default-config', 'REQ: Data for results');
  }, []);

  const history = useHistory();

  
  //If we have something from file, show graphs
  if(fileData && eventData){
    return (
			<MultiResultPageStyled>
                <DashboardStyled>
						<Card grid_area="a" icon={<DataIcon />} header="Stats">
							<InformationList>
								<InformationListEntry name="Apogee" info={apogee.toPrecision(7) + " m"} data={100}/>
                <InformationListEntry name="Touchdown velocity" info={eventData[simulationEvents[0]]["Touchdown"]["velocity"].toPrecision(4) + " m/s"} data={100}/>
								<InformationListEntry name="Highest velocity" info={maxVelocity.toPrecision(4) + " m/s"} data={100}/>
								<InformationListEntry name="Highest acceleration" info={maxAcceleration.toPrecision(4) + " m/s^2"} data={100}/>
							</InformationList>
						</Card>
						<Card grid_area="b" icon={<SettingsIcon />} header="Times">
							<InformationList>
                <InformationListEntry name="Apogee" info={eventData[simulationEvents[0]]["Apogee"]["time"].toPrecision(4) + " s"} data={100}/>
								<InformationListEntry name="Touchdown" info={eventData[simulationEvents[0]]["Touchdown"]["time"].toPrecision(4) + " s"} data={100}/>
							</InformationList>
						</Card>
						<Card grid_area="c" icon={<GraphIcon />} header="Plots">
							<TabbedChartContainer multi={true}>
								<MultiThreeDChart data={fileData} events={eventData}/>
								<MultiLineChart
										data={fileData[simulationAverages[0]]}
                    events={eventData}
										name="Altitude"
										valueField="v"
										argumentField="t"
									/>
                <BarChart
                    events={eventData}
                    name="Apogee"
                  />
								<MultiCustomLineChart fileData={fileData[simulationAverages[0]]} eventData={eventData} />
							</TabbedChartContainer>
						</Card>
                </DashboardStyled>
			</MultiResultPageStyled>
    )
  } 
  
  // Show error message if we can't find simulation data
  if(noFile){
    return(
      <div className="simulation-page">
        <h1>Results</h1>
        <p className="subtext">We couldn't find any simulation data. Have you run the simulation?</p>
        <CTAButton text="Simulate" onClick={() =>  history.push("/simulation")}/>
      </div>
    )
  }

  // Display loading status if still loading
  else{
    return(
      <div className="simulation-page">
        <h1>Loading data</h1>
        <Loader />
        <p className="subtext">Please wait...</p>
    </div>
    )
  }
}
