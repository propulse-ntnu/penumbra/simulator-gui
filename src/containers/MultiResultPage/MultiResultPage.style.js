import styled from "styled-components";

const MultiResultPageStyled =styled.div`
	margin-left: 5vw;
	height: 100vh;
	display: flex;
	align-items: center;
	justify-content: center;
`

const DashboardStyled = styled.div`
	display: grid;
	grid-template-areas: 
						"a c c c"
						"b c c c";
	column-gap: 3vw;
	row-gap: 3vw;

	width: 90%;
	height: 90%;
`

export {
	MultiResultPageStyled,
	DashboardStyled
}