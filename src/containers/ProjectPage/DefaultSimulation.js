const defaultSimulation = {
  "simulator": {
      "name": "debugsim",
      "variables": {
          "launchrod length": "5",
          "chute deployment": "500",
          "complete flight": "true",
          "stochastic": "true",
          "number of simulations": "10",
          "sim per avg": "5",
          "sample rate": "1",
          "avg and singles": "true"
      },
      "state": {
          "dimensions": "13",
          "1": "0",
          "2": "0",
          "3": "0",
          "4": "0",
          "5": "0",
          "6": "0",
          "7": "0",
          "8": "0",
          "9": "0",
          "10": "0",
          "11": "0",
          "12": "0",
          "13": "0",
          "time": "0"
      },
      "ascent": {
          "method": "RKF45",
          "time step": "0.001"
      },
      "descent": {
          "method": "RK4",
          "time step": "0.001"
      },
      "rocket": "/home/jens/PenumbraProjects/Rockets/tester.json"
  }
}

export {
	defaultSimulation
}