const rocket = {
	"cmd": "1",
	"name": "tester",
	"components": {
		"1": {
			"name": "Conical Nose Cone",
			"rho": "425",
			"length": "1",
			"r": "0.064000000000000001",
			"surface_roughness": "0.01"
		},
		"2": {
			"name": "Body Cylinder",
			"rho": "425",
			"length": "5",
			"inner_radius": "0",
			"outer_radius": "0.064000000000000001",
			"surface_roughness": "0.01"
		},
		"3": {
			"name": "Boat Tail",
			"rho": "425",
			"length": "1",
			"outer_top": "0.064000000000000001",
			"inner_top": "0",
			"outer_bottom": "0.05",
			"inner_bottom": "0",
			"surface_roughness": "0.01"
		},
		"Engine": "data\/motors\/cesaroni21062O3400-P.json",
		"Parachute": "data\/parachutes\/IrisUltra96.json"
	}
}

export {
	rocket
}