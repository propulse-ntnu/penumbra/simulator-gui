import React, {useEffect, useState} from 'react';
import CTAButton from '../../components/CTAButton';
import { useHistory } from "react-router-dom"
import Modal from '../../components/Modal/Modal';
import Loader from '../../components/loader';
import RocketBuildingPage from '../ModalContainers/RocketBuilder/RocketBuilderPage';
import SimulationConditionsPage from '../ModalContainers/SimulationConditionsPage/SimulationConditionsPage';
import SimulationAdjustmentsPage from '../ModalContainers/SimulationAdjustmentsPage/SimulationAdjustmentsPage';
import Card from '../../components/Card/Card';
import InformationList from '../../components/InformationList/informationList';
import InformationListEntry from '../../components/InformationListEntry/informationListEntry';
import { StyledPage, StyledProjectPage } from './ProjectPage.style';
import { defaultSimulation } from './DefaultSimulation';
import { rocket } from './DefaultRocketData';
import { atmosphere } from "./DefaultAtmosData";
import AtmosphereAdjustmentsPage from '../ModalContainers/AtmosphereAdjustmentsPage/AtmosphereAdjustmentsPage';
import { defaultSimulationStarter } from './DefaultSimulationStarter';

const { ipcRenderer } = window.require("electron");


export default function ProjectPage(){
  const history = useHistory();
  const [rocketData, setRocketData] = useState(rocket);
  const [atmosData, setAtmosData] = useState(atmosphere);
  const [simulationData, setSimulationData] = useState(defaultSimulation["simulator"]);
  const [showModal, setShowModal] = useState(false);
	const [modalHeader, setModalHeader] = useState("");
  const [modalContent, setModalContent] = useState(null);
  const [loading, setLoading] = useState(true);
  const [pathToSimulation, setPathToSimulation] = useState();
  const [pathToRocket, setPathToRocket] = useState();

  useEffect(() => {
    ipcRenderer.on('app:error', (event, message) => {
      console.log("ERROR", message);
    })

    ipcRenderer.on('app:obj-reply', (event, message) => {
      setSimulationData(message["simulator"]);
      setLoading(false);
    })
    setLoading(false);
    //ipcRenderer.send('app:request-json-obj', {filepath: "simulation.json"});
  }, [])

  const onButtonClick = () => {
    ipcRenderer.send('app:open-dialog');
  }


  const onModalOpen = (header ,modalCon) => {
		setModalHeader(header);
    setModalContent(modalCon);
    setShowModal(true);
  }

  const onModalClose = () => {
		console.log("Close modal")
    setShowModal(false)
  }

  const onRocketValueChanged = (values) => {
    setRocketData(values);
  }

  const onDataValuesChanged = (values) => {
    setSimulationData(values);
  }


  if(loading) {
    return(
      <div className="simulation-page">
        <h1>Loading data</h1>
          <Loader />
        <p className="subtext">Please wait...</p>
      </div>
    )
  }
  return(
    <StyledPage>
      <Modal 
				show={showModal} 
				onClose={() => onModalClose()} 
				title={modalHeader}
				content={modalContent}
			/>
      <StyledProjectPage>
					<CTAButton text="Simulate" onClick={() =>  
					{
            // ipcRenderer.send("app:save-obj-json", {data: {
            //   "cmd": "2",
            //   "name": "sim",
            //   "path": pathToSimulation          
            // }, filepath: "simulation.json"});
            console.log("Simulationdata:", simulationData);
            console.log("Rounting to multisim:", parseInt(simulationData["variables"]["number of simulations"]) > 1)
            if(parseInt(simulationData["variables"]["number of simulations"]) > 1) {
              history.push("/simulation/multisim")
            } else{
              history.push("/simulation")
            }
					}}/>
					<Card header="Rocket" onClick={() => onModalOpen("Rocket",<RocketBuildingPage data={rocketData} onChange={onRocketValueChanged} onPathChange={(name) => setPathToRocket(name)}/>)} >
            <InformationList>
							<InformationListEntry info={pathToRocket} name="Loaded rocket" />
						</InformationList>
          </Card>
          <Card header="Atmosphere" onClick={() => onModalOpen("Atmosphere", <AtmosphereAdjustmentsPage data={atmosData} onChange={onDataValuesChanged} />)}>
						<InformationList>
							<InformationListEntry info={atmosData["z0"]} name="z0" />
							<InformationListEntry info={atmosData["T0"]} name="T0" />
							<InformationListEntry info={atmosData["p0"]} name="p0" />
							<InformationListEntry info={atmosData["mu_v"]} name="mu_v" />
						</InformationList>
					</Card>
					<Card header="Simulation adjustments" onClick={() => onModalOpen("Simulation adjustments", <SimulationAdjustmentsPage data={simulationData} onChange={onDataValuesChanged} onPathChange={(name) => setPathToSimulation(name)}/>)}>
						<InformationList>
							Work in progress
						</InformationList>
					</Card>
			</StyledProjectPage>
    </StyledPage>
  )
}
