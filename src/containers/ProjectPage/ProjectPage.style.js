import styled from "styled-components";

const StyledPage = styled.div`
	display: flex;
	width: 100vw;
	height: 100vh;
	justify-content: center;
	align-items: center;
`


const StyledProjectPage = styled.div`
  display: grid;
  grid-template-columns: repeat(3, 1fr);
	grid-template-rows: repeat(2, 1fr);
  column-gap: 3vw;
	width: 90vw;
	height: 90vh;
	margin: 0 auto;
	row-gap: 3vw;
  align-items: center;
`

export {
	StyledPage,
	StyledProjectPage
}