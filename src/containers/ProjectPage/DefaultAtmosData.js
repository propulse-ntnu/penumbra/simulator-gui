const atmosphere = {
	"z0": "10",
	"T0": "273",
	"p0": "1",
	"mu_v": "100"
}

export {
	atmosphere
}