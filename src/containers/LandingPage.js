import React from 'react';
import { useHistory, Redirect } from "react-router-dom"
import ProjectCard from '../components/projectCard';
import rocket from "../static/images/Rocket.png"
import spacex from "../static/images/spacex.jpg"

export default function LandingPage() {
  return(
    <div className="center-content">
      <h1>Your projects</h1>
      <div className="landing-page center-content">
        <ProjectCard name="Stetind" edited={"30.08.21"}>
          <img className="project-image" src={rocket} alt="stetind bilde lol" />
        </ProjectCard>
        <ProjectCard name="Falcon Heavy" edited={"30.08.21"}>
          <img className="project-image" src={spacex} alt="stetind bilde lol" />
        </ProjectCard>
      </div>
    </div>  
  );
}