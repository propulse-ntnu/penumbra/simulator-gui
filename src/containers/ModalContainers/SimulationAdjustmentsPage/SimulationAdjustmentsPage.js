import { useState } from "react";
import Checkbox from "../../../components/3D/UI/Checkbox";

import InputForm from "../../../components/InputForm/InputForm";
import { TextField } from "../../../components/TextField/TextField";
import { Button } from "../../../components/Button/Button";
import { StyledButtonContainer, StyledDoubleSplit, StyledInputFields } from "./SimulationAdjustmentPage.style";

const { ipcRenderer } = window.require("electron");

export default function SimulationAdjustmentsPage(props) {
  const [data, setData] = useState(props.data);
  
  if(!props.data) {
    return null;
  }


  const onValueChange = (event, field1, field2) => {
    let da = {...data};
    if(field2){
      da[field1][field2] = event.target.value;
    } else {
      da[field1] = event.target.value;
    }
    console.log(event)

    setData(da);
    props.onChange(da);
  }

  return(
    <StyledDoubleSplit>
      <StyledInputFields>
      <h1>Variables</h1>
      <TextField type="number" text="Launchrod length" value={data["variables"]["launchrod length"]} onChange={(event) => onValueChange(event, "variables", "launchrod length")} />
      <TextField type="number" text="Chute deployment" value={data["variables"]["chute deployment"]} onChange={(event) => onValueChange(event, "variables", "chute deployment")} />
      <Checkbox checked={data["variables"]["complete flight"]} onChange={() => {
            let da = {...data};
            da["variables"]["complete flight"] = !da["variables"]["complete flight"]
            setData(da);
            props.onChange(da);
      }}/>
      <Checkbox checked={data["variables"]["stochastic"]} onChange={() => {
        let da = {...data};
        da["variables"]["stochastic"] = !da["variables"]["stochastic"]
        setData(da);
        props.onChange(da);
      }}/>
      <TextField type="number" text="Number of simulations" value={data["variables"]["number of simulations"]} onChange={(event) => onValueChange(event, "variables", "number of simulations")} />
      <TextField type="number" text="Sim per avg" value={data["variables"]["sim per avg"]} onChange={(event) => onValueChange(event, "variables", "sim per avg")} />
      <TextField type="number" text="Sample rate" value={data["variables"]["sample rate"]} onChange={(event) => onValueChange(event, "variables", "sample rate")} />
      <Checkbox checked={data["variables"]["avg and singles"]} onChange={() => {
        let da = {...data};
        da["variables"]["avg and singles"] = !da["variables"]["avg and singles"]
        setData(da);
        props.onChange(da);
      }}/>
      
      <h1>Other</h1>
        <h2>Ascent</h2>
          <TextField type="text" text="Ascent method" value={data["ascent"]["method"]} onChange={(event) => onValueChange(event, "ascent", "method")} />
          <TextField type="number" text="Step size ascent" value={data["ascent"]["time step"]} onChange={(event) => onValueChange(event, "ascent", "time step")} />
        
        <h2>Descent</h2>
          <TextField type="text" text="Descent method" value={data["descent"]["method"]} onChange={(event) => onValueChange(event, "descent", "method")} />
          <TextField type="number" text="Step size descent" value={data["descent"]["time step"]} onChange={(event) => {onValueChange(event, "descent", "time step");}} />
      </StyledInputFields>
      <StyledButtonContainer>
        <Button text="Save to file" onClick={
          () => {
              ipcRenderer.once('app:save-dialog-finished', (event, filepath) => {
                console.log("Selected file: ", filepath)
                ipcRenderer.send("app:save-obj-json-rocket", {data: data, path: filepath});
            });
            ipcRenderer.send("app:open-save-dialog");
          }
        } />

        <Button text="Load from file" onClick={
          () => {
            ipcRenderer.once('app:dialog-finished', (event, filepath) => {
              console.log(filepath);
              ipcRenderer.send("app:load-obj-json-rocket", {path: filepath});
          });
          ipcRenderer.send("app:open-dialog");
          }
        } />
      </StyledButtonContainer>
    </StyledDoubleSplit>
  )
}
