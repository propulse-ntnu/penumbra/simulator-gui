import { useState } from "react";
import Checkbox from "../../../components/3D/UI/Checkbox";

import InputForm from "../../../components/InputForm/InputForm";
import { TextField } from "../../../components/TextField/TextField";
import { Button } from "../../../components/Button/Button";
import { StyledButtonContainer, StyledDoubleSplit, StyledInputFields } from "./AtmosphereAdjustmentPage.style";

const { ipcRenderer } = window.require("electron");

export default function AtmosphereAdjustmentsPage(props) {
  const [data, setData] = useState(props.data);
  
  if(!props.data) {
    return null;
  }


  const onValueChange = (event, field1, field2) => {
    let da = {...data};
    if(field2){
      da[field1][field2] = event.target.value;
    } else {
      da[field1] = event.target.value;
    }
    console.log(event)

    setData(da);
    props.onChange(da);
  }

  return(
    <StyledDoubleSplit>
      <StyledInputFields>
      <InputForm title="Atmosphere">
          <TextField type="number" text="z0" value={data["z0"]} onChange={(event) => onValueChange(event, "z0")} />
          <TextField type="number" text="T0" value={data["T0"]} onChange={(event) => onValueChange(event, "T0")} />
          <TextField type="number" text="p0" value={data["p0"]} onChange={(event) => onValueChange(event, "p0")} />
          <TextField type="number" text="mu_v" value={data["mu_v"]} onChange={(event) => onValueChange(event, "mu_v")} />
        </InputForm>
      </StyledInputFields>
      <StyledButtonContainer>
        <Button text="Save to file" onClick={
          () => {
              ipcRenderer.once('app:save-dialog-finished', (event, filepath) => {
                console.log("Selected file: ", filepath)
                ipcRenderer.send("app:save-obj-json-rocket", {data: data, path: filepath});
            });
            ipcRenderer.send("app:open-save-dialog");
          }
        } />

        <Button text="Load from file" onClick={
          () => {
            ipcRenderer.once('app:dialog-finished', (event, filepath) => {
              console.log(filepath);
              ipcRenderer.send("app:load-obj-json-rocket", {path: filepath});
          });
          ipcRenderer.send("app:open-dialog");
          }
        } />
      </StyledButtonContainer>
    </StyledDoubleSplit>
  )
}
