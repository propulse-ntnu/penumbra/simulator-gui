import styled from "styled-components";

const StyledInputFields = styled.div`
  display: flex;
  gap: 2vh;
  flex-direction: column;
  justify-content: start;
`

const StyledButtonContainer = styled.div`
  width: 100%;
  height: 90%;
`

const StyledDoubleSplit = styled.div`
  width: 100%;
  height: 100%;
  display: grid;
  grid-template-columns: repeat(2, 1fr);
  column-gap: 3vw;
  align-items: center;
`

export {
	StyledInputFields,
	StyledButtonContainer,
	StyledDoubleSplit
}