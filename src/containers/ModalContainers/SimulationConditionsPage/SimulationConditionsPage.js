import { useState } from "react";

import InputForm from "../../../components/InputForm/InputForm";
import { TextField } from "../../../components/TextField/TextField";
import { StyledButtonContainer, StyledDoubleSplit, StyledInputFields } from "./SimulationConditionsPage.style";

import { Button } from "../../../components/Button/Button";
const { ipcRenderer } = window.require("electron");
export default function SimulationConditionsPage(props) {
  const [data, setData] = useState(props.data);
  
  if(!props.data) {
    return null;
  }


  const onValueChange = (event, field1, field2) => {
    let da = {...data};
    da[field1][field2] = event.target.value;
    setData(da);
    props.onChange(da);
  }

  return(
      <StyledDoubleSplit>
        <StyledInputFields>
          <InputForm title="Positional data">
            <TextField type="number" text="Alltitude" value={data["In"]["init_altitude"]} onChange={(event) => onValueChange(event, "In", "init_altitude")} />
          </InputForm>

          <InputForm title="Atmospheric data">
            <TextField type="number" text="Initial temperature" value={data["In"]["init_temperature"]} onChange={(event) => onValueChange(event, "In", "init_temperature")} />
            <TextField type="number" text="Initial pressure" value={data["In"]["init_pressure"]} onChange={(event) => onValueChange(event, "In", "init_pressure")} />
            <TextField type="number" text="Water pressure" value={data["In"]["water_pressure"]} onChange={(event) => onValueChange(event, "In", "water_pressure")} />
            <TextField type="text" text="Atmospheric model" value={data["In"]["atmospheric_model"]} onChange={(event) => onValueChange(event, "In", "atmospheric_model")} />
          </InputForm>
        </StyledInputFields>
        <StyledButtonContainer>
          <Button text="Save to file" onClick={
            () => {
                ipcRenderer.once('app:save-dialog-finished', (event, filepath) => {
                  console.log("Selected file: ", filepath)
                  ipcRenderer.send("app:save-obj-json-rocket", {data: data, path: filepath});
              });
              ipcRenderer.send("app:open-save-dialog");
            }
          } />

          <Button text="Load from file" onClick={
            () => {
              ipcRenderer.once('app:dialog-finished', (event, filepath) => {
                console.log(filepath);
                ipcRenderer.send("app:load-obj-json-rocket", {path: filepath});
            });
            ipcRenderer.send("app:open-dialog");
            }
          } />
        </StyledButtonContainer>
      </StyledDoubleSplit>
  )
}
