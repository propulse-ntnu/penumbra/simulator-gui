import { useEffect, useState } from "react";
import ModelBuilderDisplay from "../../../components/3D/ModelBuilderDisplay";
import Checkbox from "../../../components/3D/UI/Checkbox";

import { Button } from "../../../components/Button/Button";
import InputForm from "../../../components/InputForm/InputForm";
import { TextField } from "../../../components/TextField/TextField";
import { StyledButtonContainer, StyledDoubleSplit, StyledInputFields } from "./RocketBuilderPage.style";

const { ipcRenderer } = window.require("electron");


export default function RocketBuildingPage(props) {
  const [rocketData, setRocketData] = useState(props.data)

  const onValueChange = (event, bodyPart, parameter1, parameter2) => {
    let roc = {...rocketData};
    if(parameter2 != null) {
      roc["components"][bodyPart][parameter1][parameter2] = Number(event.target.value);
    } else {
      roc["components"][bodyPart][parameter1] = Number(event.target.value);
    }

    setRocketData(roc);
    props.onChange(roc);
  }

  useEffect(() => {
    ipcRenderer.on('app:rocket-reply', (event, rocket) => {
      setRocketData(rocket);
    })
  })

  // Iterate through the comfile and create a form
  const createTextFields = () => {
    const fields = []
    const components = Object.keys(rocketData["components"])
    components.forEach(component => {
      console.log(component);
      fields.push(<h1>{rocketData["components"][component]["name"]}</h1>)
      let attribute = rocketData["components"][component]

      // Render upload button on Engine or Parachute
      if(component == "Engine" || component == "Parachute") {
        fields.push(
          <Button text={`Upload ${component}`} onClick={
            () => {
              ipcRenderer.once('app:dialog-finished', (event, message) => {
                  let da = {...rocketData};
                  da["components"][component]= message
                  setRocketData(da);
                  props.onChange(da);
              });
              ipcRenderer.send("app:open-dialog");
            } 
          } />
        )
        fields.push(<p>{rocketData["components"][component]}</p>)
        return
      }

      Object.keys(attribute).forEach(attr => {
        //Detached is the only field needing a checkbox
        if(attr == "detached") {
          fields.push( 
          <Checkbox title={attr} checked={rocketData["components"][component][attr]} onChange={() => {
            let da = {...rocketData};
            da["components"][component][attr] = !da["components"][component][attr]
            setRocketData(da);
            props.onChange(da);
          }}/>)
          return
        }

        if(attr == "name") {
          return
        }

        let value = rocketData["components"][component][attr]
        // Go deeper
        if(attr == "CM" || attr == "I_s") {
          Object.keys(value).forEach(inner_attr => {
            fields.push(<TextField type="number" text={inner_attr} value={rocketData["components"][component][attr][inner_attr]} onChange={(event) => onValueChange(event, component, attr, inner_attr)} />)
          })
        } else {
          fields.push(<TextField type="number" text={attr} value={rocketData["components"][component][attr]} onChange={(event) => onValueChange(event, component, attr, null)} />)
        }
      })
    })
    return fields
  }


  return(
      <StyledDoubleSplit>
        <StyledInputFields>
          {createTextFields()}
        </StyledInputFields>
        <StyledButtonContainer>
          <Button text="Save to file" onClick={
            () => {
                ipcRenderer.once('app:save-dialog-finished', (event, filepath) => {
                  console.log("Selecte file: ", filepath)
                  ipcRenderer.send("app:save-obj-json-rocket", {data: rocketData, path: filepath});
              });
              ipcRenderer.send("app:open-save-dialog");              
            }
          } />

          <Button text="Load from file" onClick={
            () => {
              ipcRenderer.once('app:dialog-finished', (event, filepath) => {
                console.log(filepath);
                ipcRenderer.send("app:load-obj-json-rocket", {path: filepath});
            });
            ipcRenderer.send("app:open-dialog");
            }
          } />
        </StyledButtonContainer>
      </StyledDoubleSplit>
  )
}
