import { useState } from "react"
import { Button } from "../Button/Button"
import { DropdownButton } from "../DropdownButton/DropdownButton";
import { StyledDropdownMenu, StyledDropdownMenuContainer } from "./DropdownMenu.style"

// type = "button"?
const DropdownMenu = ({text, children, showMenu, onClick}) => {
	return(
		<StyledDropdownMenuContainer>
			<DropdownButton text={text} onClick={onClick} />
			{showMenu ?
			(
			<StyledDropdownMenu>
				{children}
			</StyledDropdownMenu>
			) :
				null
			}
		</StyledDropdownMenuContainer>
	)
}

export {
	DropdownMenu
}