import styled from "styled-components";

const colors = {
	primary: "#6564DB",
	text: "#242732",
	lightgrey: "#494d5e",
}

// .button?
const StyledDropdownMenu = styled.div`
	background-color: #242732;
	box-shadow: 1vh 1vh 0.2vh 0vh #00000040;
	border-radius: 1vh;
	height: 40vh;
	overflow-y: scroll;
	overflow-x: hidden;
	position: relative;

	&::-webkit-scrollbar {
  width: 0.25vw;
	}

	&::-webkit-scrollbar-thumb {
		background: ${colors.lightgrey};
	}
`

const StyledDropdownMenuContainer = styled.div`
	display: flex;
	flex-direction: column;
	align-items: center;
	width: fit-content;
	position: fixed;
	right: 10vw;
	z-index: 1;
`

// .p?
const StyledDropdownMenuText = styled.p `
	color: ${colors.text};
	font-size: 3vh;
	font-weight: bold;
	margin: 0;
`

export {
	StyledDropdownMenu,
	StyledDropdownMenuContainer
}