import react from "react"

// The call to action button. Typically as a yellow floating button in bottom corner
export default function CTAButton(props){

  return(
    <div className="cta-button button floating-medium" onClick={props.onClick}>
      <p className="button-text">
        {props.text}
      </p>
    </div>
  )
}

