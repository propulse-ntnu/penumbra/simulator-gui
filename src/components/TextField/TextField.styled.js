import styled from "styled-components";

const colors = {
	textGrey: "#B1B1B1",
	white: "#FFFFFF",
	fieldGrey: "#494D5E"
} 

const StyledTextField = styled.input`
	color: ${colors.white};
	background: ${colors.fieldGrey};
	border: none;
	height: 3vh;
	width: 50%;
	border-radius: 0.2rem;
	padding-left: 0.6rem;
	font-size: 1.7vh;
`

const StyledTextFieldHeader = styled.h1`
	color: ${colors.white};
	font-size: 2vh;
`

export {
	StyledTextField,
	StyledTextFieldHeader
}