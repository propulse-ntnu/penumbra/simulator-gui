import { StyledTextField, StyledTextFieldHeader } from "./TextField.styled"


const TextField = ({text, type, value, onChange}) => {
	return(
		<>
			<StyledTextFieldHeader>{text}</StyledTextFieldHeader>
			<StyledTextField type={type} value={value} onChange={onChange} />
		</>
	)
}

export {
	TextField
}