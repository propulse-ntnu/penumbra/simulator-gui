import { StyledButton, StyledButtonText } from "./Button.style"


const Button = ({onClick, text}) => {
	return(
		<StyledButton type="button" onClick={onClick}>
			<StyledButtonText>{text}</StyledButtonText>
		</StyledButton>
	)
}

export {
	Button
}