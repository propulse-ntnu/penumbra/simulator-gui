import styled from "styled-components";

const colors = {
	primary: "#6564DB",
	text: "#242732"
}

const StyledButton = styled.button`
	background: ${colors.primary};
	border: none;
	border-radius: 0.2rem;
	padding: 1vh;
	width: fit-content;
	margin: 1vh;
	box-shadow: 0.4vh 0.4vh 0.4vh 0vh #00000040;
	transition: transform 200ms ease;

	&:hover {
		transform: scale(1.05);
	}
`

const StyledButtonText = styled.p `
	color: ${colors.text};
	font-size: 3vh;
	font-weight: bold;
	margin: 0;
`

export {
	StyledButton,
	StyledButtonText
}