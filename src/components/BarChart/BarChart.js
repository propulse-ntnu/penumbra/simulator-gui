import React, {useEffect, useRef, useState} from "react"
// import { DropdownMenuItem } from "../DropdownItem/DropdownMenuItem";
// import { DropdownMenu } from "../DropdownMenu/DropdownMenu";
// import LineChart from "../LineChart/LineChart";
import Plot from "react-plotly.js"
import * as d3 from 'd3';

import {colors} from "./BarChart.style"

export default function BarChart(props) {
    const events = props.events;
    const [width, setWidth] = useState(300);
    const [height, setHeight] = useState(300);
    const stageRef = useRef(null);
    let xAxisMinValue = null;
    let xAxisMaxValue = 0;
    let [xAxisValues, setXAxisValues] = useState([]);
    let [yAxisValues, setYAxisValues] = useState([]);
    const bars = [];
    const value = "value";
    const amount = "amount";

    useEffect(() => {
      
      // Define number of steps
      const steps = 10;

      // Sets min and max apogee
      Object.keys(events).forEach(event => {
        let apogee = events[event]["Apogee"][value]

        if (xAxisMinValue == null) {xAxisMinValue = apogee; console.log("Found lowest apogee:", apogee)}
        if (apogee > 0 && apogee < xAxisMinValue) {xAxisMinValue = apogee}
        if (apogee > xAxisMaxValue) {xAxisMaxValue = apogee}
      })

      // Define value per step
      const xAxisIntervalValue = xAxisMaxValue - xAxisMinValue;
      const xAxisStepValue = xAxisIntervalValue / steps;

      // Sets the barwidth within the xAxisInterval
      for (let step = 0; step < steps; step++) {
        bars.push({
          value: xAxisMinValue + step*xAxisStepValue,
          amount: 0
        })
      }
      
      // Counts amount of apogees within each bar
      Object.keys(events).forEach(event => {
        let apogee = events[event]["Apogee"][value]
        for (let step = steps-1; step > -1; step--) {
          if (apogee >= bars[step][value]) {
            bars[step][amount] = bars[step][amount] + 1
            return;
          };
        }
      })

      // Sets the barwidth within the xAxisInterval
      for (let step = 0; step < steps; step++) {
        xAxisValues.push(bars[step][value])
      }

      // Sets the height within the yAxisInterval
      for (let step = 0; step < steps; step++) {
        yAxisValues.push(bars[step][amount])
      }

      setXAxisValues(xAxisValues);
      setYAxisValues(yAxisValues);

    }, [])

    useEffect(() => {
      if(stageRef.current) {
       setHeight(stageRef.current.offsetHeight);
       setWidth(stageRef.current.offsetWidth);
      }
    }, [stageRef])

    var trace = {
      type: 'bar',
      x: xAxisValues,
      y: yAxisValues,
      marker: {
        color: '#6564DB',
        opacity: 0.6,
        line: {
          color: '#6463DA',
          width: 1.5
        }
      }
    };

    var data = [trace];

    return(
      <div className="chart-generic" ref = {stageRef}>
        <Plot
          data={data}
          layout={{
            font:{
              color:"white"
            },
            legend:{
              font: {
                color: "white",
                size:16
              }
            },
            title: {
              text: 'Apogee',
              font: {
                color: 'white'
              }
            },
            xaxis: {
              title: {
                text: 'Apogee (m)',
                font: {
                  family: 'Courier New, monospace',
                  size: 18,
                  color: 'white'
                }
              }
            },
            yaxis: {
              title: {
                text: 'Amount (n)',
                font: {
                  family: 'Courier New, monospace',
                  size: 18,
                  color: 'white'
                }
              }
            },
            paper_bgcolor:"#242732",
            plot_bgcolor:"#242732",
            barmode: 'stack'
          }}
        />
      </div>
    )
    }

export {
	BarChart
}