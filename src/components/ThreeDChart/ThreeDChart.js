import { useState, useRef, useEffect} from "react";
import Plot from "react-plotly.js"

export default function ThreeDChart(props){
  const stageRef = useRef(null);
	const [width, setWidth] = useState(300);
	const [height, setHeight] = useState(300);


	const x = props.data.map(d => d["x"])
	const y = props.data.map(d => d["y"])
	const z = props.data.map(d => d["z"])

	let trace = {
		x: x,
		y: y,
		z: z,
		type: 'scatter3d',
		mode: 'lines',
		marker: {
			color: "#5B58F0",		// F0666B
			size: 30
		}
	}

	const totalAverage = [trace]

	useEffect(() => {
		if(stageRef.current) {
			setHeight(stageRef.current.offsetHeight);
      setWidth(stageRef.current.offsetWidth);

			console.log(width, height)
		}
	}, [stageRef])

	console.log(width)
  return(
    <div className="chart-generic" ref = {stageRef}>
      <Plot
				data={totalAverage}
				layout={{
					font:{
						color:"white"
					},
					legend:{
						font: {
							color: "#8CD867",
							size: 8
						}
					},
					paper_bgcolor:"#242732",
				}}
			/>
    </div>
  )
}