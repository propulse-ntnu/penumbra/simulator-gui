import * as THREE from "three";
import ThreeCamera from "./Camera";

export default class ThreeCanvas {
  constructor(panning) {
    this._scene = new THREE.Scene();
    this._scene.background = new THREE.Color('#87ceeb');
    this._scene.fog = new THREE.Fog(this._scene.background, 5000, 5000*100)
    

    this.renderer = new THREE.WebGLRenderer({ antialias: true });
    this.renderer.setSize(500, 500);
    this.canvas = this.renderer.domElement;
    this._camera = new ThreeCamera(this.renderer);

    this.renderer.shadowMap.enabled = true;
    this.renderer.shadowMap.type = THREE.PCFSoftShadowMap;
	//	this.renderer.outputEncoding = THREE.sRGBEncoding;

    this.gameObjects = [];
  }

  addGameObjectToScene(gameObject) {
    this.gameObjects.push(gameObject);
    this._scene.add(gameObject.mesh);
  }

  removeGameObjectFromScene(gameObject) {
    this._scene.remove(gameObject.mesh);
  }

  animate() {
    this.resizeCanvasToDisplaySize();
    this._camera.animate();
    for(let i = 0; i < this.gameObjects.length; i++){
      this.gameObjects[i].animate();
    }
    this.renderer.render(this._scene, this._camera.camera);
  }


  resizeCanvasToDisplaySize() {
    // look up the size the canvas is being displayed
    const width = this.canvas.clientWidth;
    const height = this.canvas.clientHeight;
    // adjust displayBuffer size to match
    if (this.canvas.width !== width || this.canvas.height !== height) {
      // you must pass false here or three.js sadly fights the browser
      this.renderer.setSize(width, height, false);
      this._camera.camera.aspect = width / height;
      this._camera.camera.updateProjectionMatrix();
    }
  }

  getCanvas() {
    return this.canvas;
  }

  get scene(){
    return this._scene;
  }

  set scene(scene){
    this._scene = scene
  }

  get camera() {
    return this._camera;
  }

  getRenderer() {
    return this.renderer;
  }
}