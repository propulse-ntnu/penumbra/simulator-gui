import * as THREE from "three";


export default class GameObject {
  constructor(mesh, animation) {
    this._mesh = mesh;
    this._animation = animation;
    this._position = new THREE.Vector3();
  }

  get mesh(){
    return this._mesh;
  }

  set mesh(mesh){
    this._mesh = mesh
  }

  get position(){
    return this._mesh.position;
  }

  set position(position) {
    this._mesh.position.copy(position)
  }

  set animation(animation) {
    this._animation = animation;
  }

  addChild(child){
    this._mesh.add(child)
  }

  animate() {
    this._animation();
  }
}
