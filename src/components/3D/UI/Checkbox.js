export default function Checkbox(props) {
  return(
    <label>
      <input 
        type="checkbox"
        checked={props.checked}
        onChange={props.onChange}
      />
      {props.text}
    </label>
  )

}