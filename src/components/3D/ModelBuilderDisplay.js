import { useEffect, useRef, useState} from "react";
import OrbitControls from "three-orbitcontrols"
import * as THREE from "three";

import ThreeCanvas from "./Canvas";
import Rocket from "./Rocket";

var rocket;
var canvas;

export default function ModelBuilderDisplay(props){
  const modelRef = useRef(null);

  useEffect(() => {
    console.log("Update")
    canvas = new ThreeCanvas();

    modelRef.current.appendChild(canvas.getCanvas());

    //LIGHTS
    var lights = [];
    lights[0] = new THREE.PointLight(0xffffff, 1, 0);
    lights[1] = new THREE.PointLight(0xffffff, 1, 0);
    lights[2] = new THREE.PointLight(0xffffff, 1, 0);
    lights[0].position.set(0, 0, 0);
    lights[1].position.set(100, 200, 100);
    lights[2].position.set(-100, -200, -100);
    for(let i = 0; i < lights.length; i++) {
      canvas.scene.add(lights[i])
    }

    rocket = new Rocket(props.rocketData);
    canvas.addGameObjectToScene(rocket);

    var animate = function () {
      canvas.animate();
      requestAnimationFrame( animate );
    };
    animate();
  }, [])

  useEffect(() => {
    if(rocket){
      canvas.removeGameObjectFromScene(rocket)
    }
    rocket.rocketData = props.rocketData
    canvas.addGameObjectToScene(rocket)
  }, [props.rocketData])

  return(
    <>
      <div className="model-container center-content" ref={modelRef} />
    </>
  )
}