
import GameObject from "./GameObject";

import * as THREE from "three";

export default class Rocket extends GameObject {
  constructor(rocketData) {
    super(null, () => {})
    this._rocketData = rocketData;
    this._size = {x:0, y:0, z:0};
    if(rocketData) {
      try{
        this.createModelFromData();
      } catch (error) {
        return
      }
    }
  }

  set rocketData(data) {
    this._rocketData = data;
    this.createModelFromData();
  }

  get size(){
    return this._size;
  }

  createModelFromData() {
    const material = new THREE.MeshPhongMaterial({color: 0x000000})

    let coneGeometry = new THREE.ConeGeometry(
      this._rocketData.cone.radius,
      this._rocketData.cone.height,
      64,
      64);

    this.cone = new THREE.Mesh(coneGeometry, material);

    let bodyGeometry = new THREE.CylinderGeometry(
      this._rocketData.body.radius,
      this._rocketData.body.radius,
      this._rocketData.body.height,
      64, 64
    )

    this.body = new THREE.Mesh(bodyGeometry, material)
    this.body.position.y -= this._rocketData.cone.height/2 + this._rocketData.body.height/2; //Automatically adjsut the position of the body to be directly under

    const group = new THREE.Group();
    group.add(this.cone);
    group.add(this.body)

    this._size.y = this._rocketData.cone.height + this._rocketData.body.height;

    this.mesh = group;
  }
}