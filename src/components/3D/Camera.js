import OrbitControls from "three-orbitcontrols";
import * as THREE from "three";
import { Object3D } from "three";


const distance = 20;

export default class ThreeCamera {
  constructor(renderer){
    this._camera = new THREE.PerspectiveCamera( 50, 1, 0.1, 5000*100);
    this._camera.position.x = distance;

    this.controls = new OrbitControls(this._camera, renderer.domElement);


    this._target = new Object3D();
    this._lookDirection = new THREE.Vector3();
  }

  set target(target) {
    this._target = target;
    this._camera.lookAt(target.position);
    
  }

  get camera() {
    return this._camera
  }

  animate() {
  }
}