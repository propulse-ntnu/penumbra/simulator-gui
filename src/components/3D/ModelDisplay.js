import { useEffect, useRef, useState} from "react";
import OrbitControls from "three-orbitcontrols"
import * as THREE from "three";


import Loader from "../loader";
import ThreeCanvas from "./Canvas";
import Rocket from "./Rocket";

var canvas;

export default function ModelDisplay(props){
  const modelRef = useRef(null);
  const [loadingModel, setLoadingModel] = useState(true);

  useEffect(() => {
    canvas = new ThreeCanvas();
    canvas.camera.pan = true

    modelRef.current.appendChild(canvas.getCanvas());

    //LIGHTS
    var lights = [];
    lights[0] = new THREE.PointLight(0xffffff, 1, 0);
    lights[1] = new THREE.PointLight(0xffffff, 1, 0);
    lights[2] = new THREE.PointLight(0xffffff, 1, 0);
    lights[0].position.set(0, 0, 0);
    lights[1].position.set(100, 200, 100);
    lights[2].position.set(-100, -200, -100);
    for(let i = 0; i < lights.length; i++) {
      canvas.scene.add(lights[i])
    }

    var animate = function () {
      canvas.animate();
      requestAnimationFrame( animate );
    };
    animate();
  }, [])

  // When updating where the model is saved, display loading 
  useEffect(() => {
    setLoadingModel(true);
    const rocket = new Rocket(props.path, () => {
      setLoadingModel(false);
      canvas.scene.add(rocket.mesh)
    });
  }, [props.path])

  return(
    <>
      <div className="model-container center-content" ref={modelRef} >
      {loadingModel
        ?
        <> 
          <Loader />
          <p>Loading {props.path}</p>
        </>
        : <></>
      }
      </div>
    </>
  )
}