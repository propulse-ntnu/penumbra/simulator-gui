import { StyledTabMenu, StyledTabMenuItem, StyledTabMenuText } from "./TabMenu.style";

const TabMenu = (props) => {
	return(
		<StyledTabMenu>
			{props.children}
		</StyledTabMenu>
	)
}

// The tab button of charts
const TabItem =({text, onClick, active}) => {

	return(
		<StyledTabMenuItem onClick={onClick} isActive={active}>
			<StyledTabMenuText>{text}</StyledTabMenuText>
		</StyledTabMenuItem>
	)
}

export {
	TabMenu,
	TabItem
};