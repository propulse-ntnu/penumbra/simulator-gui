import styled from "styled-components";

const colors = {
	activeItem: "#494D5E"
}

const StyledTabMenu = styled.div`
	display: flex;
	flex-direction: row;
	align-items: center;
	width: 100%;
	height: fit-content;
`

const StyledTabMenuItem = styled.div`
	width: fit-content;
	height: fit-content;
	margin-left: 1vw;
	cursor: pointer;
	background-color: ${props => props.isActive ? colors.activeItem : "none"};
	border-radius: 0.2vw;
	padding: 0.5vw;
	transition: transform 400ms ease;

	&:hover {
		transform: ${props => !props.isActive ? "scale(1.05)" : "scale(1)"};
	}
`

const StyledTabMenuText = styled.div`
	font-size: 1vw;
`

export {
	StyledTabMenu,
	StyledTabMenuItem,
	StyledTabMenuText
}