import styled from "styled-components";

const StyledInputForm = styled.div`
	width: 100%;
	height: fit-content;
	display: flex;
	flex-direction: column;
	margin-bottom: 2vh;
	padding: 1vh;
`

const StyledInputHeader = styled.h1`
	font-size: 3.5vh;
	margin: 0;
`

export {
	StyledInputForm,
	StyledInputHeader
}