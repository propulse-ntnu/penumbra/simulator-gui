import { StyledInputForm, StyledInputHeader } from "./InputForm.style";

const InputForm = ({name, children}) => {
	return(
		<StyledInputForm>
			<StyledInputHeader>{name}</StyledInputHeader>
			{children}
		</StyledInputForm>
	)
}

export default InputForm;