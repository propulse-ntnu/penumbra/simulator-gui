import { useHistory } from "react-router-dom"

// Card used to populate project page
export default function ProjectCard(props){
    let history = useHistory();
    const onClick = () => {
      history.push("/project/home")
    }

    return(
      <div className="card project-card floating-medium" onClick={onClick}>
        <div className="project-image-container">
          {props.children}
        </div>
        <div>
          <h1 className="project-title">{props.name}</h1>
          <h1 className="project-subtitle">Last edited: {props.edited}</h1>
        </div>
      </div>
    )
}