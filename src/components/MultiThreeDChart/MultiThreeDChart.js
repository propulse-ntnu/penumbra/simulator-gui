import { useState, useRef, useEffect} from "react";
import Plot from "react-plotly.js"

const range_of_dots = 1000;

export default function MultiThreeDChart(props){
  const stageRef = useRef(null);
	const [width, setWidth] = useState(300);
	const [height, setHeight] = useState(300);

	const averageData = props.data;

	// Data from total average file ("avg51") - Issues with reading from file, so it reads from "avg1"
	const x = props.data["avg1"].map(d => d["x"])
	const y = props.data["avg1"].map(d => d["y"])
	const z = props.data["avg1"].map(d => d["z"])

	console.log(props.events);

	const landing_spots_y = Object.keys(props.events).map(event => {
		let data = props.events[event]
		return(data["Touchdown"]["y"]);
	});

	console.log("Landing spots y: ", landing_spots_y);
	const landing_spots_x = new Array(landing_spots_y.length).fill(0);
	console.log("Landing spots x: ", landing_spots_x);

	const landing_spots_z = Object.keys(props.events).map(event => {
		let data = props.events[event]
		if(Math.abs(data["Touchdown"]["z"]) > 5000) {
			return -2000
		}
		return(data["Touchdown"]["z"]);
	});

	console.log("Landing spots z: ", landing_spots_z);

	const dot_trace = {
		name: "Landing spots",
		x: landing_spots_z,
		y: landing_spots_y,
		z: landing_spots_x,
		type: 'scatter3d',
		mode: 'markers',
		marker: {
			color: '#8CD867',
			size: 5,
			symbol: 'circle',
			line: {
			color: 'rgb(204, 204, 204)',
			width: 1},
			opacity: 0.8}
	}

	// Main plot
	const mainTrace = {
		name: "Position",
		x: x,
		y: y,
		z: z,
		type: 'scatter3d',
		mode: 'lines',
		marker: {
			color: "#5B58F0",		// F0666B
			size: 30
		}
	}

	const totalAverage = [dot_trace, mainTrace];

	Object.keys(averageData).forEach(average => {
        
		// Change criterion to match name of total average filename
		if (average != "avg51") {
			const x = averageData[average].map(d => d["x"])
			const y = averageData[average].map(d => d["y"])
			const z = averageData[average].map(d => d["z"])

			let trace = {					// "avg42" and "avg 43" have some issues, the rest works
				x: x,
				y: y,
				z: z,
				type: 'scatter3d',
				mode: 'lines',
				opacity: 0.3,
				marker: {
					color: "#6564DB",
					size: 30
				}
			}

			totalAverage.push(trace);
		}
    })


	useEffect(() => {
		if(stageRef.current) {
			setHeight(stageRef.current.offsetHeight);
      		setWidth(stageRef.current.offsetWidth);
		}
	}, [stageRef])

  return(
    <div className="chart-generic" ref = {stageRef}>
      <Plot
				data={totalAverage}
				layout={{
					font:{
						color:"white"
					},
					legend:{
						font: {
							color: "#8CD867",
							size: 8
						}
					},
					paper_bgcolor:"#242732",
				}}
			/>
    </div>
  )
}
