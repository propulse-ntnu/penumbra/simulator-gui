import { StyledDropdownButton, StyledButtonText, StyledSvgContainer } from "./DrodownButton.style"
import { ReactComponent as DropdownIcon } from "../../static/icons/arrow_drop_down_black_24dp.svg";


const DropdownButton = ({onClick, text}) => {
	return(
		<StyledDropdownButton type="button" onClick={onClick}>
			<StyledButtonText>{text}</StyledButtonText>
			<StyledSvgContainer><DropdownIcon /></StyledSvgContainer>
		</StyledDropdownButton>
	)
}

export {
	DropdownButton
}