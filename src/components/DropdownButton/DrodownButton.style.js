import styled from "styled-components";

const colors = {
	primary: "#6564DB",
	text: "#242732"
}

const StyledDropdownButton = styled.button`
	display: flex;
	background: ${colors.primary};
	border: none;
	border-radius: 0.2rem;
	padding: 0.2vh;
	width: fit-content;
	height: fit-content;
	box-shadow: 0.4vh 0.4vh 0.4vh 0vh #00000040;
	transition: transform 200ms ease;

	&:hover {
		transform: scale(1.05);
	}
`

const StyledSvgContainer = styled.div`
	margin: auto;
	fill: ${colors.text};
	margin-left: 0.5vw;
`

const StyledButtonText = styled.p `
	color: ${colors.text};
	font-size: 1.5vh;
	font-weight: bold;
	margin: 0;
	margin: auto;
	margin-left: 0.5vw;
`

export {
	StyledDropdownButton,
	StyledSvgContainer,
	StyledButtonText
}