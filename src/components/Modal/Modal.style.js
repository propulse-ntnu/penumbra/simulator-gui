import styled from "styled-components";

const colors = {
	grey: "#242732",
	lightgrey: "#494d5e",
	white: "#F6F4F3",
	red: "#D64045",
} 

const StyledModal = styled.div`
	transition: transform 200ms ease;
  background-color: ${colors.grey};
	box-shadow: 1vh 1vh 0.2vh 0vh #00000040;
  height: 80vh;
	width: 80vw;
  border-radius: 0.2vw;
  visibility: ${props => props.active ? "visible" : "hidden"};
	position: absolute;
	z-index: 15;
`

const StyledModalContent = styled.div`
	display: flex;
	flex-direction: row;
	overflow: scroll;
	overflow-x: hidden;
	padding-left: 1vw;
	padding-right: 1vw;
	height: 71vh;


	&::-webkit-scrollbar {
  width: 0.25vw;
	}

	&::-webkit-scrollbar-thumb {
		background: ${colors.lightgrey};
	}


`


const StyledModalHeader = styled.div`
	display: flex;
	align-items: center;
	fill: ${colors.white};
	margin: 0;
	margin-bottom: 1vh;
	height: 3vh;
	padding: 1.5rem;
	box-shadow: 0vh 0.6vh 0.2vh 0vh #00000040;
	overflow: hidden;
`

const StyledModalHeaderText = styled.h1`
	font-size: 4vh;
	margin: 0;
	margin-left: 1vw;
`

const StyledCloseButton = styled.div`
	fill: ${colors.red};
	margin-left: auto;
	height: 100%;

	transition: transform 200ms ease;

	&:hover {
		transform: scale(1.1);
		cursor: pointer;
	}
`

export {
	StyledModal,
	StyledModalContent,
	StyledModalHeader,
	StyledModalHeaderText,
	StyledCloseButton
}