import {ReactComponent as CloseIcon} from "../../static/icons/close_black_24dp.svg"
import { StyledCloseButton, StyledModal, StyledModalContent, StyledModalHeader, StyledModalHeaderText } from "./Modal.style"

//Component shown as popup when clicking other component.
export default function Modal({show, onClose, title, content}){

	console.log("Content" + content)

	return(
		<StyledModal active={show}>
			<StyledModalHeader>
				<StyledModalHeaderText>
					{title}
				</StyledModalHeaderText>
				<StyledCloseButton />
				<StyledCloseButton>
					<CloseIcon onClick={onClose}/>
				</StyledCloseButton>
			</StyledModalHeader>
			<StyledModalContent>
				{content}
			</StyledModalContent>
		</StyledModal> 
	)
}
