import styled from "styled-components";

const StyledTabbedChartDisplay = styled.div`
	width:100%;
	height:80%;
`

export {
	StyledTabbedChartDisplay
}