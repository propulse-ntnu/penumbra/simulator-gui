import {useState, useEffect} from "react"
import { StyledTabbedChartDisplay } from "./TabbedChartDisplay.style"
import {TabMenu, TabItem} from "../TabMenu/TabMenu"

// The white box which displays the chart
export default function TabbedChartContainer(props){
  
  const [chart, setChart] = useState(null)
  const [chartNumber, setChartNumber] = useState(0)


  useEffect(() => {
    console.log(chart)
    if(chart == null){
      setChart(props.children[chartNumber])
    }
  }, [chart])

  const changeChart = (chartValue) => {
    setChart(null);
    setChartNumber(chartValue)
  }

  if(props.multi) {
    return(
      <StyledTabbedChartDisplay>
        <TabMenu>
          <TabItem active={0 == chartNumber} onClick={() => {changeChart(0)}} text="Position"/>
          <TabItem active={1 == chartNumber} onClick={() => {changeChart(1)}} text="Velocity"/>
          <TabItem active={2 == chartNumber} onClick={() => {changeChart(2)}} text="Apogee"/>
          <TabItem active={3 == chartNumber} onClick={() => {changeChart(3)}} text="Custom"/>
        </TabMenu>
        {chart}
      </StyledTabbedChartDisplay>
    )
  } else {
    return(
      <StyledTabbedChartDisplay>
        <TabMenu>
          <TabItem active={0 == chartNumber} onClick={() => {changeChart(0)}} text="Position"/>
          <TabItem active={1 == chartNumber} onClick={() => {changeChart(1)}} text="Velocity"/>
          <TabItem active={2 == chartNumber} onClick={() => {changeChart(2)}} text="Custom"/>
        </TabMenu>
        {chart}
      </StyledTabbedChartDisplay>
    )
  }

}