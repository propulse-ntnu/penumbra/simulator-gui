import { scaleTime } from "d3"
import styled from "styled-components"

const colors = {
	grey: "#242732",
	white: "#F6F4F3"
} 

const StyledCard = styled.div`
	transition: transform 200ms ease;
  background-color: ${colors.grey};
	box-shadow: 0.2vh 0.2vh 0.2vh 0vh #00000040;
  height: 100%;
  border-radius: 0.2vw;
  display: block;
	overflow: hidden;
	grid-area: ${(props) => props.grid_area};
	transition: transform 200ms ease;

	&:hover {
		transform: ${props => props.onClick ? "scale(1.05)" : "scale(1.0)"};
		cursor: ${props => props.onClick ? "pointer" : "unset"};
	}
`

const StyledCardHeader = styled.div`
	display: flexbox;
	align-items: center;
	fill: ${colors.white};
	margin: 0;
	margin-bottom: 1vh;
	height: 3vh;
	padding: 1.5rem;
	box-shadow: 0vh 0.6vh 0.2vh 0vh #00000040;
	overflow: hidden;
`

const StyledHeaderText = styled.h1`
	font-size: 2vh;
	margin: 0;
	margin-left: 1vw;
`

export {
	StyledCard,
	StyledCardHeader,
	StyledHeaderText
}