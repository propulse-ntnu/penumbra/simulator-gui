import { StyledCard, StyledCardHeader, StyledHeaderText } from "./Card.style"
import { ReactComponent as GraphIcon } from "../../static/icons/show_chart_black_48dp.svg"

const Card = ({icon, header, children, onClick, grid_area}) => {
	return (
		<StyledCard onClick={onClick} grid_area={grid_area}>
			<StyledCardHeader>
				{icon}
				<StyledHeaderText>
					{header}
				</StyledHeaderText>
			</StyledCardHeader>
			{children}
		</StyledCard>
	)
}

export default Card;