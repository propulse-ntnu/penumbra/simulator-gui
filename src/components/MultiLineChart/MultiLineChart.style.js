import styled from "styled-components";

const colors = {
	green: "#8CD867",
	white: "#f6f4f3",
	grey: "#BDBAB8",
	purple: "#6564DB",
	red: "#D64045",
}

const StyledMultiLineChart = styled.div`

`

export {
	colors
}