import React, {useEffect, useRef, useState} from "react"
import * as d3 from 'd3';

import {colors} from "./MultiLineChart.style"

export default function MultiLineChart(props) {

  const [data, setData] = useState(props.data)
  const [events, setEvent] = useState(props.events)
  const [width, setWidth] = useState(0);
  const [height, setHeight] = useState(0);
	const [selectedEvent, setSelectedEvent] = useState("")
	const [selectedArgument, setSelectedArgument] = useState(0)
	const [selectedValue, setSelectedValue] = useState(0)

  const d3Chart = useRef()
  

  function drawChart() {
    const margin = {
      top: 20, 
      right: 10, 
      bottom: 60, 
      left: 70
    }
    d3.select(d3Chart.current).selectAll('*').remove();
    
    setWidth(parseInt(d3.select('.chart-generic').style('width')) - margin.left - margin.right);
    setHeight(parseInt(d3.select('.chart-generic').style('height')) - margin.top - margin.bottom);
    const backgroundColor = d3.select('.chart-generic').style('background-color')
    const plotColor = colors.purple
    const axisColor = colors.grey


    //Set up chart
    const svg = d3.select(d3Chart.current)
                    .attr('width', width)
                    .attr('height', height)
                    .style('background-color', backgroundColor)
                    .style('stroke', axisColor)
                    .style('color', axisColor)
                    .append('g')
                    .attr('transform', 'translate(' + margin.left + ',' + margin.top + ')')
    


    // Define x axis
    const x = d3.scaleLinear()
                .domain(d3.extent(data, (d) => {return d[props.argumentField]}))
                .range([0, width])
    var xAxis = svg.append("g")
                .attr("transform", "translate(0," + height + ")")
                .call(d3.axisBottom(x));
    
    // Define y axis
    const y = d3.scaleLinear()
        .domain(d3.extent(data, (d) => {return d[props.valueField]}))
        .range([height, 0])

    // Create the y axis in the svg
    svg.append('g')
      .attr("fill", axisColor)
      .call(d3.axisLeft(y))

    // Add X axis label:
    svg.append("text")
      .attr("text-anchor", "end")
      .attr("x", width)
      .attr("y", height + margin.top + 20)
      .attr("font-size", "10px")
      .attr("fill", "red")
      .text("Time (s)");

    // Y axis label:
    svg.append("text")
      .attr("text-anchor", "end")
      .attr("transform", "rotate(-90)")
      .attr("y", -margin.left+18)
      .attr("x", -margin.top)
      .attr("font-size", "10px")
      .text("Test (m)")

    // Create line
    var line = d3.line()
                  .x((d) => {return x(d[props.argumentField])})
                  .y((d) => {return y(d[props.valueField])})
    
    // Add a tooltip div. Here I define the general feature of the tooltip: stuff that do not depend on the data point.
    // Its opacity is set to 0: we don't see it by default.
    var tooltip = d3.select(".chart-generic")
      .append("div")
      .style("opacity", 1)
      .style("background-color", "#1B1D2A")
      .style("border", "solid")
      .style("border-width", "1px")
      .style("border-radius", "5px")
      .style("padding", "5px")
      .style("width", "14%")
      .style("height", "8%")
      .style("position", "absolute")
      .style("font-size", "60%")
      .style("color", "#7F7EF5")

    // Add a clipPath: everything out of this area won't be drawn.
    var clip = svg.append("defs").append("svg:clipPath")
      .attr("id", "clip")
      .append("svg:rect")
      .attr("width", width )
      .attr("height", height )
      .attr("x", 0)
      .attr("y", 0);

    // A function that change this tooltip when the user hover a point.
    // Its opacity is set to 1: we can now see it. Plus it set the text and position of tooltip depending on the datapoint (d)
    // function mouseover(d) {
    //   tooltip.style("opacity", 1)
    // }

	  function mousemove(event) {
      // recover coordinate we need
      var x0 = x.invert(d3.pointer(event)[0]);                   
      if (x0 < 0) {return null}
      var i = d3.bisect(data.map(d => d[props.argumentField]), x0) - 1;            // Recovers index of the correct object
      var selectedData = data[i]
      focusLine 
        .attr("x", x(selectedData[props.argumentField]))
        .attr("y", y(selectedData[props.valueField]))
        .attr("height", height - y(selectedData[props.valueField]))
      focusPoint
        .attr("cx", x(selectedData[props.argumentField]))
        .attr("cy", y(selectedData[props.valueField]))
      setSelectedArgument(selectedData[props.argumentField])
      setSelectedValue(selectedData[props.valueField])
      focusLine.style("opacity", 1)
      focusPoint.style("opacity", 1)
      var roundedValueField = Math.round(selectedData[props.valueField])
      var roundedArgumentField = Math.round(selectedData[props.argumentField])
      var currentEvent = ""
      for (let i = 0; i < events.length; i++) {
        if (selectedData[props.argumentField] >= events[i].t - 1 && selectedData[props.argumentField] <= events[i].t + 1) {currentEvent = events[i].event}
      }
      tooltip
        .html("" + props.valueField + ": " + roundedValueField + "<br>" + props.argumentField + ": " + roundedArgumentField + "<br>Event: " + currentEvent)
        .style("left", (x(selectedData[props.argumentField]) + 90) + "px") // It is important to put the +90: other wise the tooltip is exactly where the point is an it creates a weird effect
        .style("top", (y(selectedData[props.valueField] + 10)) + "px")
        .style("opacity", 1)
  	}


	function mouseout() {
    focusLine.style("opacity", 0)
		focusPoint.style("opacity", 0)
    tooltip
      .transition()
      .duration(200)
      .style("opacity", 0)
	}

    // Add brushing
    var brush = d3.brushX()                 // Add the brush feature using the d3.brush function
    .extent( [ [0,0], [width,height] ] )  // initialise the brush area: start at 0,0 and finishes at width,height: it means I select the whole graph area
    .on("end", updateChart)               // Each time the brush selection changes, trigger the 'updateChart' function
		

    // Add brushing to "g"
    svg.append('g')
      .attr("class", "brush")
      .call(brush)
			// .on('mouseover', mouseover)
      .on('mousemove', mousemove)
      .on('mouseout', mouseout);
		
    //Create plot
    const path = svg
        .append('path')
        .datum(data)
        .attr('fill', 'none')
        .attr("clip-path", "url(#clip)")
        .attr('stroke', plotColor)
        .attr('stroke-width', 3)
        .attr('d', line)

    // Create the line that travels along the curve of chart
    var focusLine = svg
      .append('g')
      .append('rect')
        .attr("class", "dotted")
        .attr("stroke-width", "1px")
        .attr("width", ".5px")
        .attr("y", height)
				.attr("color", colors.grey)

    // Create the circle that travels along the curve of chart
    var focusPoint = svg
      .append('g')
      .append('circle')
        .attr("stroke", "black")
        .attr('r', 3)
				.attr("fill", colors.white)
				.attr("stroke-width", 0)


		//Create event circles
		svg
		.append("g")
		.selectAll("circle")
		.data(events)
		.enter()
		.append("circle")
			.attr("class", "event-dot")
			.attr("cx", (event) => {return x(event.t)})
			.attr("cy", (event) => {
				var i = d3.bisect(data.map(d => d[props.argumentField]), event.t) - 1;
				return y(data[i][props.valueField])
			})
			.attr("r", 5)
			.on("mouseover", (mouseover, ev) => {setSelectedEvent(ev["event"])})
			.attr("fill", colors.red)
			.attr("stroke-width", 0)


    // A function that set idleTimeOut to null
    var idleTimeout
    function idled() { idleTimeout = null; }

    // A function that update the chart for given boundaries
    function updateChart(event) {

      var extent = event.selection

      // If no selection, back to initial coordinate. Otherwise, update X axis domain
      if(!extent){
        if (!idleTimeout) return idleTimeout = setTimeout(idled, 1000); // This allows to wait a little bit
        x.domain(d3.extent(data, (d) => {return d[props.argumentField]}))
      }
      else{
        x.domain([ x.invert(extent[0]), x.invert(extent[1]) ])
        svg.select(".brush").call(brush.move, null) // This remove the grey brush area as soon as the selection has been done
      }

      xAxis.style("color", "black")

      // Update axis position
      xAxis
        .transition()
        .duration(1000)
        .call(d3.axisBottom(x))
      

      // Create line
      var line = d3.line()
        .x((d) => {return x(d[props.argumentField])})
        .y((d) => {return y(d[props.valueField])})

      path
        .transition("fade").duration(1000)
        .attr("d", line)


      // Update circle positions
      svg.selectAll(".event-dot")
      .data(events)
			.join('circle')
      .transition()
      .duration(1000)
			.attr("cx", (event) => {return x(event[props.argumentField])})
			.attr("cy", (event) => {
				var i = d3.bisect(data.map(d => d[props.argumentField]), event[props.argumentField]) - 1;
				return y(data[i][props.valueField])
			})
    }
  }

  useEffect(() => {
    drawChart();
    window.addEventListener('resize', drawChart);
  }, [width, height, props.valueField]);

  return(
    <div className="chart-generic">
      <svg ref={d3Chart} />
    </div>
  );
}