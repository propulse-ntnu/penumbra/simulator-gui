import React from "react"

import Navbarlink from "../NavbarLink/NavbarLink"
import { StyledNavbar } from "./Navbar.style"
import Logo from "../../../static/images/Logo.png"
import { ReactComponent as GraphIcon } from "../../../static/icons/show_chart_black_48dp.svg"
import { ReactComponent as RocketIcon} from "../../../static/icons/rocket_launch_black_24dp.svg"

const Navbar = () => {
	return(
		<StyledNavbar>
				<Navbarlink to="/project/home">
					<img src={Logo} alt="Logo" />
				</Navbarlink>
				<Navbarlink to="/project/results">
					<GraphIcon />
				</Navbarlink>
				<Navbarlink to="/project/realtime">
					<RocketIcon />
				</Navbarlink>
		</StyledNavbar>
	)
};

export {
	Navbar
}