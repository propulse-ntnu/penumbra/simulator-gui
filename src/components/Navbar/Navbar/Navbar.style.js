import styled from "styled-components"

const colors = {
	default: "#242732"
}

const StyledNavbar = styled.nav`
	position: fixed;
  background-color: ${colors.default}; 
	box-shadow: 0.2vh 0.2vh 0.2vh 0vh #00000040;

	display: flex;
	flex-direction: column;
	align-items: center;

  height: 100vh;
  width: 5vw;
  top: 0;
  left: 0;
`

export {
	StyledNavbar
}