import React from 'react';
import { NavLink,useLocation } from 'react-router-dom'
import { StyledActiveIndicator, StyledNavbarLink } from './NavbarLink.style';

export default function Navbarlink(props){
	var location = useLocation();
	var isActive = location.pathname === props.to
	if(isActive){
		return <ActiveNavbarLink props={props}/>
	}	
	return <DefaultNavbarLink props={props}/>
};

function ActiveNavbarLink(props){
	return(
		<NavLink to={props.props.to}>
			<StyledNavbarLink active>
					{props.props.children}
			</StyledNavbarLink>
		</NavLink>
	);
}

function DefaultNavbarLink(props){
	return(
		<NavLink to={props.props.to}>
			<StyledNavbarLink>
					{props.props.children}			
			</StyledNavbarLink>
		</NavLink>
	);
}
