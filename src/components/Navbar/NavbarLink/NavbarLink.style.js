import styled from "styled-components";

const colors = {
	inactive: "#494D5E",
	hover: "#7D839C",
	active: "#6564DB",
	red: "#D64045",
}

const StyledNavbarLink = styled.div`
	fill: ${props => props.active ? colors.active : colors.inactive};

	display: flex;
	margin: 0 auto;
	align-items: center;
	justify-content: center;
	width: 100%;
	height: 4vh;
	margin-top: 2vh;

	text-decoration: none;

	transition: transform 200ms ease;

	&:hover {
		fill: ${colors.hover};
		transform: scale(1.1);
	}
`

export {
	StyledNavbarLink,
}