import react from "react"

// The default loader
export default function Loader(){
  return(
    <div class="loader">
      <div></div>
      <div></div>
      <div></div>
      <div></div>
    </div>
  )
}