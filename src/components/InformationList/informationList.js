// Contains information highligts. Should be populated by InformationListEntry

import { InformationListStyled } from "./InformationList.style";

export default function InformationList(props){
  return(
    <InformationListStyled>
      {props.children}
		</InformationListStyled>
  )
}