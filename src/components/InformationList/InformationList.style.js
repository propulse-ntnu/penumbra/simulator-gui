import styled from "styled-components";

const InformationListStyled = styled.div`
	display: flex;
  flex-direction: column;
	margin-top: 0.5vh;

	width: 100%;
	height: 100% - 3vh;
`

export {
	InformationListStyled
}