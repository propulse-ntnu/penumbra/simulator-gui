import { useState } from "react";
import { DropdownMenuItem } from "../DropdownItem/DropdownMenuItem";
import { DropdownMenu } from "../DropdownMenu/DropdownMenu";
import LineChart from "../LineChart/LineChart";
import { StyledChartTitle } from "./CustomLineChart.style";

const CustomLineChart = ({fileData, eventData}) => {
	const [dropDownValue, setDropdownValue] = useState("v");
	const [showDropDown, setShowDropDown] = useState(false);

  const handleValueFieldChange = (valueField) => {
		console.log(valueField);
    setDropdownValue(valueField)
		setShowDropDown(false);
  }

	const generateMenuItems = () => {
		var menuItems = []
		var possibleValues = Object.keys(fileData[0])
		for(var i=0; i < possibleValues.length; i++) {
			menuItems.push(
				<DropdownMenuItem name={possibleValues[i]} onClick={handleValueFieldChange}/>
			);
		}
		return menuItems
	}

	return(
		<>
			<DropdownMenu text="Y-axis value" showMenu={showDropDown} onClick={() => {setShowDropDown(!showDropDown)}}>
				{generateMenuItems()}
			</DropdownMenu>
			<StyledChartTitle>{dropDownValue}</StyledChartTitle>
			<LineChart
				data={fileData}
				events={eventData}
				name="Altitude"
				valueField={dropDownValue}    // {valueField}
				argumentField="t"
			/>
		</>
	)
}

export {
	CustomLineChart
}