import styled from "styled-components";

const colors = {
	primary: "#6564DB",
	text: "#242732"
}

const StyledChartTitle = styled.h1`
	background-color: ${colors.text};
	text-align: center;
	font-size: 1.5vw;
	margin-left: 2vw;
`

export {
	StyledChartTitle
}