import styled from "styled-components";

const colors = {
	main: "#6564DB",
	white: "#F6F4F3",
	grey: "#BDBAB8"
}

const InformationListEntryStyled = styled.div`
	display: flex;
  align-items: center;
  flex-direction: row;
	margin-bottom: 0.5vh;
	margin-top: 0.5vh;
	height: 20%;
`

const InfoTextContainer = styled.div`
  display: flex;
  flex-direction: column;
  justify-content: centers;
`

const InfoIndicator = styled.div`
	background-color: ${colors.main};
	
	width: 0.3vw;
	height: 5vh;
	margin-right: 1.5vw;
	margin-left: 1.0vw;
	border-radius: 0.3rem;
`

const PrimaryTextStyled = styled.h1`
	font-size: 1.0vw;
	margin: 0.6vh;
	color: ${colors.white};
`

const SecondaryTextStyled = styled.h1`
	font-size: 0.7vw;
	margin: 0.5vh;
	margin-top: 0rem;
	color: ${colors.grey};
`


export {
	InformationListEntryStyled,
	InfoTextContainer,
	InfoIndicator,
	PrimaryTextStyled,
	SecondaryTextStyled
}