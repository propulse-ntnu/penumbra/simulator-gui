import { useState, useEffect } from "react";

import Loader from "../loader";
import { InfoIndicator, InformationListEntryStyled, InfoTextContainer, PrimaryTextStyled, SecondaryTextStyled } from "./InformationListEntry.style";

// Displays the value in the default-config object with the same name as passed in props.name
// Show loading if calculating default-config
export default function InformationListEntry({name, info}){
	return(
		<InformationListEntryStyled>
			<InfoIndicator />
			<InfoTextContainer>
				<PrimaryTextStyled>{info}</PrimaryTextStyled>
				<SecondaryTextStyled>{name}</SecondaryTextStyled>
			</InfoTextContainer>
		</InformationListEntryStyled>
	)
}