import { StyledDropdownMenuItem, StyledDropdownMenuItemText } from "./DropdownMenuItem.style"


const DropdownMenuItem = ({name, onClick}) => {
	return(
		<StyledDropdownMenuItem onClick={() => onClick(name)}>
			<StyledDropdownMenuItemText>{name}</StyledDropdownMenuItemText>
		</StyledDropdownMenuItem>
	)
}

export {
	DropdownMenuItem
}