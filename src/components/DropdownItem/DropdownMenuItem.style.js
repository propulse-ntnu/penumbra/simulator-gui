import styled from "styled-components";

const colors = {
	primary: "#6564DB",
	text: "#242732"
}

// .button?
const StyledDropdownMenuItem = styled.div`
	background-color: #242732;
	width: 100%;
	padding: 0.5vh;
	&:hover {
		background-color: #494D5E;
	}
`

const StyledDropdownMenuItemText = styled.p`
	color: #FFFFFF;
	font-size: 2vh;
	margin: 0;
`

export {
	StyledDropdownMenuItem,
	StyledDropdownMenuItemText
}