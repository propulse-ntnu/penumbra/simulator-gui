const os = require('os')

const platforms = {
  WINDOWS: 'WINDOWS',
  MAC: 'MAC',
  LINUX: 'LINUX',
};

const platformsNames = {
  win32: platforms.WINDOWS,
  darwin: platforms.MAC,
  linux: platforms.LINUX,
};

// Returns which platform the app is being run on
const getCurrentPlatform = () => {
  return platformsNames[os.platform()]
} 

const getHomePath = () => {
  return os.homedir() + "/";
}

module.exports = {
  platforms,
  getCurrentPlatform,
  getHomePath
}