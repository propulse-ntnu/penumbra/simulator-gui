const path = require("path")

const { getCurrentPlatform, platforms } = require('./OsReader');

const app = require("electron").app;
const isDev = require('electron-is-dev');
const { exec } = require("child_process");
const {dialog} = require("electron");

const app_folder = app.getAppPath()


// Run the simulation command based on which OS the user is running
const runSimulation = (callback) => {
  let run_command = ""

  console.log(`[${getCurrentPlatform()}] Running simulation....`)
	console.log("App folder: " + __dirname);
	console.log("USER HOME IS: ", app.getPath("home"));

	path_to_exe = path.join(process.resourcesPath, "extraResources", "Linux")
  
  if(getCurrentPlatform() == platforms.LINUX){
		if(isDev) {
			path_to_exe = path.join("extraResources", "Linux")
		}
    run_command = "cd " + path_to_exe + ";" + "./penumbra"
  } else{
		if(isDev) {

			path_to_exe = path.join("extraResources", "Windows")
		} else {
      path_to_exe = path.join(process.resourcesPath, "extraResources", "Windows")
    }
    run_command = "cd " + path_to_exe + " && " + "penumbra.exe"
  }
  
    exec(run_command, (error, stdout, stderr) => {
      if(error){
        callback(new Error("Could not simulation executable while running command: \n" + run_command))
				console.log(error)
      }
      callback(stdout)
  })
}

// Open dialog window for selecting file to upload
const openDialogWindow = (callback) => {
  let filePath = dialog.showOpenDialog({ properties: ['openFile', 'multiSelections'] });
  filePath
    .then(res => {
      let filePath = res["filePaths"][0]
      console.log("File path selected: ", filePath)
      callback(filePath);
    })
}

const saveDialogWindow = (callback) => {
  let filePath = dialog.showSaveDialog({
    title: 'Select save file',
    filters: [
     { name: 'All Files', extensions: ['json'] }
    ]});
  filePath
    .then(res => {
      let filePath = res["filePath"]
      callback(filePath);
    })
}

module.exports = {
  runSimulation,
  openDialogWindow,
  saveDialogWindow
}
