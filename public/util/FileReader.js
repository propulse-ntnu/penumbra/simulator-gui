// This file contains functions for reading different filetypes on the OS.

const fs = require('fs');
const app = require("electron").app;
const path = require("path")
const isDev = require('electron-is-dev');
const { getCurrentPlatform, platforms } = require('./OsReader');


function getPathToFiles() {
  var path_to_files = "";
  switch(getCurrentPlatform()) {
    case platforms.WINDOWS:
      path_to_files = path.join("extraResources", "Windows")
      if(!isDev) {
        path_to_files = path.join(process.resourcesPath, "extraResources", "Windows")
      }
      break;
    case platforms.LINUX:
      path_to_files = path.join("extraResources", "Linux")
      if(!isDev) {
        path_to_files = path.join(process.resourcesPath, "extraResources", "Linux")
      }
      break;
  }
  return path_to_files;
}

function readHeaderLine(headerLine){
  const line = headerLine //To remove new line char
  const lineArr = line.split(",")
  let headerObj = {}
  for(let i = 0; i < lineArr.length; i++){
    headerObj[i] = lineArr[i]
  }
  return headerObj
}

function csvToString(fileName) {
  console.log("reading " + fileName)

  if(fs.existsSync(fileName)){
    let data = fs.readFileSync(fileName, 'utf8'); // Open datastream and look at each line as an arrayentry
    return data
  } else{
    throw "File doesn't exist"
  }
}

function readEvents(fileName) {
  let data = csvToString(fileName).split('\n'); // Open datastream and look at each line as an arrayentry
  let fileData = {};
  let data_obj = {};
  const headerObj = readHeaderLine(data[0]); //Read the headers, setting the correct values in the object
  for(let i=1; i<data.length-1; i++){ //Iterate through each line and populate obj values
    data_obj = {}
    let lineArr = data[i].split(",");
    data_obj = {
      "Apogee": {
        "time": parseFloat(lineArr[1]),
        "value": parseFloat(lineArr[0])
      },
      "Max speed": {
        "value": parseFloat(lineArr[2])
      },
      "Touchdown": {
        "time": parseFloat(lineArr[4]),
        "velocity": parseFloat(lineArr[3]),
        "y": parseFloat(lineArr[5]),
        "z": parseFloat(lineArr[6])
      }
    }
    fileData[`event${i}`] = data_obj
  }
  console.log("Loaded ", Object.keys(fileData).length, "datapoints")
  return fileData;
}

function readSingleCsv(fileName) {
  let data = csvToString(fileName).split('\n'); // Open datastream and look at each line as an arrayentry
  let fileData = []
  let data_obj = {};
  const headerObj = readHeaderLine(data[0]); //Read the headers, setting the correct values in the object
  for(let i=1; i<data.length-1; i++){ //Iterate through each line and populate obj values
    data_obj = {}
    let lineArr = data[i].split(",");
    for(let j=0; j<Object.keys(headerObj).length; j++){
      const num = parseFloat(lineArr[j])
      data_obj[headerObj[j]] = num
    }
    fileData.push(data_obj);
  }
  console.log("Loaded ", fileData.length, "datapoints")
  return fileData;
}

function readCsvs(){
  const statsDir = path.join(app.getPath("home"), "PenumbraProjects", "Results", "simulation", "stats");
  const files = fs.readdirSync(statsDir);
  const average_files = files.filter((file) => {
    return file.includes("avg");
  })
  const event_file = "events.csv"

  const averages = {}
  let i = 1
  average_files.forEach((file) => {
    averages[`avg${i}`] = readSingleCsv(path.join(statsDir, file))
    i++;
  })

  const events = readEvents(path.join(statsDir, event_file))
  console.log("Read files: ", average_files);
  console.log("Read events:", events);

  return(
    {
      events: events,
      averages: averages
    }
  )
}

function writeObjectToJson(obj, filepath) {  
  console.log("Writing to:", filepath)
  fs.writeFile(filepath, JSON.stringify(obj), (error) => {
    if (error) throw error;
    else {
      console.log("Successfully saved to ", filepath)
    }
  })
}

function readJsonFromAbsolutePaths(filepath) {
  return new Promise((resolve, reject) => {
    fs.readFile(filepath, 'utf8', (err, jsonString) => {
      if(err) {
        reject(err);
      }  
      const obj = JSON.parse(jsonString)
      resolve(obj)
    })
  })
}

function readJsonToObj(filepath) {
  const path_to_files = getPathToFiles();
  console.log(path_to_files);
  return new Promise((resolve, reject) => {
    fs.readFile(path_to_files + "/" + filepath, 'utf8', (err, jsonString) => {
      if(err) {
        reject(err);
      }  
      const obj = JSON.parse(jsonString)
      resolve(obj)
    })
  })
}

module.exports = {
  readCsvs,
  csvToString,
  readJsonToObj,
  readJsonFromAbsolutePaths,
  writeObjectToJson
}
