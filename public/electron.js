const path = require('path');

const { app, BrowserWindow, ipcMain } = require('electron');
const isDev = require('electron-is-dev');

const { readCsvs, readJsonToObj, writeObjectToJson, csvToString, readJsonFromAbsolutePaths } = require("./util/FileReader");
const { runSimulation, openDialogWindow, saveDialogWindow } = require("./util/SystemInterface")
const { getHomePath } = require("./util/OsReader");

function createWindow() {
  // Create the browser window.
  const win = new BrowserWindow({
    title: "Penumbra",
    width: 1000,
    height: 1000,
    webPreferences: {
      contextIsolation: false,
      nodeIntegration: true,
      enableRemoteModule: true,
    }
  });
  win.removeMenu();

  // and load the index.html of the app.
  // win.loadFile("index.html");
  win.loadURL(
    isDev
      ? 'http://localhost:3000/#/project/multiresults'
      : `file://${path.join(__dirname, '../build/index.html')}`
  );
  // Open the DevTools.
  //if (isDev) {
    win.webContents.openDevTools();
  //}
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.whenReady().then(createWindow);

// Quit when all windows are closed, except on macOS. There, it's common
// for applications and their menu bar to stay active until the user quits
// explicitly with Cmd + Q.
app.on('window-all-closed', () => {
  if (process.platform !== 'darwin') {
    app.quit();
  }
});

app.on('activate', () => {
  if (BrowserWindow.getAllWindows().length === 0) {
    createWindow();
  }
});

/////////////////////////////////////////

// Call to open filedialog
//Run ipcRenderer.send('app:open-dialog', ""); to call this function
ipcMain.on("app:open-dialog", (event, arg) => {
  console.log("Recieved request to open file dialog")
  openDialogWindow((res) => {
    event.reply("app:dialog-finished", res)
  })
})

ipcMain.on("app:open-save-dialog", (event, arg) => {
  console.log("Recieved request to open file dialog")
  saveDialogWindow((res) => {
    event.reply("app:save-dialog-finished", res)
  })
})

ipcMain.on("app:get-home", (event, arg) => {
  
  const home = getHomePath();
  console.log("gethome", home);
  event.reply("app:get-home", home);
})

// Call to run simulation
//Run ipcRenderer.send('app:run-sim', ""); to call this function
ipcMain.on('app:run-sim', (event, arg) => {
  console.log("Recieved request to run sim")
    runSimulation((res) => {
      if(res instanceof Error){
        console.log("Sim failed")
        event.reply('app:sim-failed', res.message)
      } else{
        console.log("Sim finished")
        event.reply('app:sim-finished', "Finished sim")
      }
    });

  }
)

// Call this in render component when you need default-config
//Run ipcRenderer.send('app:request-default-config', ""); to call this function
ipcMain.on('app:request-default-config', (event, arg) => {
  try{
    const fileContent = readCsvs()
    event.reply('app:file-reply', fileContent);
  } catch(e) {
    console.log(e.message)
    event.reply('app:no-file')
  } 
});

ipcMain.on('app:get-file-string', (event, arg) => {
  try{
    const fileContent = csvToString(path.join(app.getPath("home"), "PenumbraProjects", "results", "test.csv"))
    event.reply('app:file-string-reply', fileContent);
  } catch(e) {
    console.log(e.message)
    event.reply('app:no-file')
  } 
});

// Call this in render component when you need rocket default-config
//Run ipcRenderer.send('app:request-rocket', ""); to call this function
ipcMain.on('app:request-json-obj', (event, arg) => {
  try{
    readJsonToObj(arg.filepath).then(obj => {
      console.log(obj)
      event.reply('app:obj-reply', obj);
    })
  } catch(e) {
    console.log(e.message)
    event.reply('app:error', e)
  } 
});

ipcMain.on('app:save-obj-json', (event, arg) => {
  try{
    writeObjectToJson(arg.data, path.join(app.getPath("home"), "PenumbraProjects", "Results", "simulation", "stats", "simulation.json"));
    event.reply('app:successful save');
  } catch(e) {
    console.log(e.message)
    event.reply('app:error', e)
  } 
});


ipcMain.on('app:save-obj-json-rocket', (event, arg) => {
  try{
    writeObjectToJson(arg.data, arg.path);
    event.reply('app:successful save');
  } catch(e) {
    console.log(e.message)
    event.reply('app:error', e)
  } 
});

ipcMain.on('app:load-obj-json-rocket', (event, arg) => {
  try{
    readJsonFromAbsolutePaths(arg.path).then(obj => {
      event.reply("app:rocket-reply", obj)
    });
  } catch(e) {
    console.log(e.message)
    event.reply('app:error', e)
  } 
});